import { useMemo } from "react";
import Head from "next/head";
import { getHeadTitle } from "@/utils/head";
import { NextPage } from "next";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { skipToken } from "@reduxjs/toolkit/dist/query";

import { Box, CircularProgress } from "@mui/material";

import { useFetchDistributorsQuery } from "@/api";
import DefaultTemplateV2 from "@/templates/DefaultV2";

const DistributorLoadingPage: NextPage = () => {
  const headTitle = useMemo(() => getHeadTitle("Distribuidoras"), []);
  const router = useRouter();
  const { data: session, status } = useSession();

  if (session && session.user.universityId === undefined) {
    router.push("/instituicoes");
  }

  const { data: distributors } = useFetchDistributorsQuery(
    session?.user.universityId ?? skipToken
  );

  if (distributors) {
    router.push(
      `/distribuidoras/${distributors!.length > 0 ? distributors[0].id : -1}`
    );
  }

  if (status === "loading") {
    return (
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        height="100vh"
      >
        <CircularProgress />
      </Box>
    );
  }

  if (status === "unauthenticated") {
    router.push("/");
    return null;
  }

  return (
    <DefaultTemplateV2>
      <Head>
        <title>{headTitle}</title>
      </Head>
      <Box
        height="100%"
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <CircularProgress />
      </Box>
    </DefaultTemplateV2>
  );
};

export default DistributorLoadingPage;
