interface ContractData {
  peakContractedDemandInKw: number;
  offPeakContractedDemandInKw: number;
}

interface ConsumptionHistoryPlot {
  date: string[];
  peakConsumptionInKwh: number[];
  offPeakConsumptionInKwh: number[];
  peakMeasuredDemandInKw: (number | null)[];
  offPeakMeasuredDemandInKw: number[];
}

export interface ConsumerUnitGraphResponse {
  contractData: ContractData;
  consumptionHistoryPlot: ConsumptionHistoryPlot;
}