import { ReactNode, useEffect, useState } from "react";
import { Box, Container, ContainerProps } from "@mui/material";
import Drawer from "@/components/Drawer";
import Header from "@/components/Header";
import Footer from "@/components/Footer";
import { useSelector } from "react-redux";
import { selectIsDrawerOpen } from "@/store/appSlice";

type DefaultTemplateProps = {
  headerAction?: ReactNode;
  secondaryDrawer?: ReactNode;
  contentHeader?: ReactNode;
  contentContainerMaxWidth?: ContainerProps["maxWidth"];
  children?: ReactNode;
};

const DefaultTemplateV2 = ({
  headerAction,
  secondaryDrawer,
  contentHeader,
  children,
}: DefaultTemplateProps) => {
  const isDrawerOpen = useSelector(selectIsDrawerOpen);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [width, setWidth] = useState("calc(100vw - 64px)");

  // Atualizar a largura com transição suave ao abrir/fechar o drawer
  useEffect(() => {
    setWidth(isDrawerOpen ? "calc(100vw - 64px)" : "80vw");
  }, [isDrawerOpen]);

  return (
    <Box display="flex" height="100vh">
      {/* Drawer Lateral */}
      <Drawer />

      {/* Conteúdo Principal */}
      <Box
        width="100vw" // Garante que ocupe toda a largura da tela
        flexGrow={1}
        sx={{
          transition: "width 0.3s ease-in-out",
        }}
      >
        {/* Cabeçalho */}
        <Header>{headerAction}</Header>

        {/* Conteúdo Secundário e Principal */}
        <Box display="flex">
          {secondaryDrawer && (
            <Box
              height="calc(100vh - 64px)"
              sx={{
                transition: "transform 8.3s ease-in-out", // Suaviza a entrada/saída do drawer secundário
              }}
            >
              {secondaryDrawer}
            </Box>
          )}

          {/* Conteúdo Principal */}
          <Box
            flexGrow={1}
            minHeight="calc(100vh - 64px)"
            maxHeight="calc(100vh - 64px)"
            overflow="auto" // Substituir overlay por auto para maior compatibilidade
            display="flex"
            flexDirection="column"
          >
            <Box sx={{ flexGrow: 1, position: "relative", pb: 5 }}>
              {contentHeader}

              {/* Container Principal */}
              <Container
                maxWidth={false} // Permite que o container ocupe toda a largura
                sx={{ height: "100%", width: "100%" }} // Garante 100% de largura
              >
                {children}
              </Container>
            </Box>

            {/* Rodapé */}
            <Footer />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default DefaultTemplateV2;
