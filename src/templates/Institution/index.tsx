import { useMemo, useState } from "react";
import { useFetchInstitutionsQuery } from "@/api";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Box,
  TextField,
  InputAdornment,
  TableSortLabel,
} from "@mui/material";
import InstitutionEditButton from "./EditButton";
import { GridSearchIcon } from "@mui/x-data-grid";

const InstitutionsTemplate = () => {
  const { data: institutions } = useFetchInstitutionsQuery();

  const [searchQuery, setSearchQuery] = useState("");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);

  const [order, setOrder] = useState<"asc" | "desc">("asc");
  const [orderBy, setOrderBy] = useState<string>("name");

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (property: string) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const filteredInstitutions = useMemo(() => {
    if (!institutions) return [];

    const filtered = institutions.filter((institution) => {
      const acronym = institution?.acronym?.toLowerCase() || "";
      const cnpj = institution.cnpj.toLowerCase() || "";
      const name = institution.name.toLowerCase() || "";
      const searchString = searchQuery.toLowerCase();

      return (
        acronym.includes(searchString) ||
        cnpj.includes(searchString) ||
        name.includes(searchString)
      );
    });

    return filtered.sort((a, b) => {
      const isAsc = order === "asc";
      const aName = a.name || "";
      const bName = b.name || "";
      const aAcronym = a.acronym || "";
      const bAcronym = b.acronym || "";
      const aCnpj = a.cnpj || "";
      const bCnpj = b.cnpj || "";

      if (orderBy === "name") {
        return isAsc ? aName.localeCompare(bName) : bName.localeCompare(aName);
      }
      if (orderBy === "acronym") {
        return isAsc ? aAcronym.localeCompare(bAcronym) : bAcronym.localeCompare(aAcronym);
      }
      if (orderBy === "cnpj") {
        return isAsc ? aCnpj.localeCompare(bCnpj) : bCnpj.localeCompare(aCnpj);
      }

      return 0;
    });
  }, [institutions, searchQuery, order, orderBy]);


  return (
    <TableContainer>
      <Box>
        <Box
          display="flex"
          justifyContent="flex-end"
          alignItems="center"
          mb={2}
          sx={{
            backgroundColor: "#0A5C67",
            paddingTop: "10px",
            marginBottom: "0px",
          }}
        >
          <TextField
            placeholder="Buscar"
            variant="standard"
            size="small"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <GridSearchIcon sx={{ color: "white" }} />
                </InputAdornment>
              ),
              sx: {
                color: "white",
                "&:before": {
                  borderBottomColor: "white",
                },
              },
            }}
            InputLabelProps={{
              style: { color: "white" },
            }}
            sx={{ width: "200px", marginRight: "16px" }}
          />
        </Box>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell width="122px" sortDirection={orderBy === "acronym" ? order : false}>
                <TableSortLabel
                  active={orderBy === "acronym"}
                  direction={orderBy === "acronym" ? order : "asc"}
                  onClick={() => handleRequestSort("acronym")}
                >
                  Sigla
                </TableSortLabel>
              </TableCell>
              <TableCell sortDirection={orderBy === "name" ? order : false}>
                <TableSortLabel
                  active={orderBy === "name"}
                  direction={orderBy === "name" ? order : "asc"}
                  onClick={() => handleRequestSort("name")}
                >
                  Nome
                </TableSortLabel>
              </TableCell>
              <TableCell width="166px" sortDirection={orderBy === "cnpj" ? order : false}>
                <TableSortLabel
                  active={orderBy === "cnpj"}
                  direction={orderBy === "cnpj" ? order : "asc"}
                  onClick={() => handleRequestSort("cnpj")}
                >
                  CNPJ
                </TableSortLabel>
              </TableCell>
              <TableCell width="48px"></TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {filteredInstitutions?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((institution) => (
              <TableRow
                key={institution.id}
                style={{
                  textDecoration: institution.isActive ? "none" : "line-through",
                  color: "inherit",
                }}
              >
                <TableCell>{institution.acronym}</TableCell>
                <TableCell>{institution.name}</TableCell>
                <TableCell>{institution.cnpj}</TableCell>
                <TableCell>
                  <InstitutionEditButton institutionId={institution.id} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>

        <TablePagination
          rowsPerPageOptions={[10, 25, 50]}
          component="div"
          count={filteredInstitutions?.length || 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          sx={{
            "& .MuiTablePagination-toolbar": {
              backgroundColor: "#0A5C67",
            },
            "& .MuiTablePagination-select, & .MuiTablePagination-selectIcon, & .MuiTablePagination-actions, & .MuiTablePagination-displayedRows": {
              color: "#FFFFFF",
            },
            "& .MuiIconButton-root": {
              color: (theme) =>
                page === 0 && rowsPerPage >= filteredInstitutions.length
                  ? theme.palette.action.disabled
                  : theme.palette.primary.contrastText,
              "&.Mui-disabled": {
                color: (theme) => theme.palette.action.disabled,
              },
            },
            "& .MuiTablePagination-selectLabel, & .MuiTablePagination-displayedRows": {
              color: "#FFFFFF",
            },
          }}
        />
      </Box>
    </TableContainer>
  );
};

export default InstitutionsTemplate;