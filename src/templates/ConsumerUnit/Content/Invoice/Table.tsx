import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { skipToken } from "@reduxjs/toolkit/dist/query";
import { format } from "date-fns";
import { ptBR } from "date-fns/locale";
import { formatNumberConditional, formatToPtBrCurrency } from "@/utils/number";
import { Button, Grid, IconButton, Tooltip } from "@mui/material";
import {
  CancelRounded,
  CheckCircleOutlineRounded,
  InsightsRounded,
  WarningRounded,
  Edit,
  Delete,
} from "@mui/icons-material";

import {
  useFetchInvoicesQuery,
  useDeleteEnergiBillMutation,
  useGetConsumerUnitQuery,
} from "@/api";
import {
  selectActiveConsumerUnitId,
  selectConsumerUnitInvoiceActiveFilter,
  selectConsumerUnitInvoiceDataGridRows,
  setConsumerUnitInvoiceDataGridRows,
  setEnergyBillEdiFormParams,
  setIsEnergyBillCreateFormOpen,
  setIsEnergyBillEdiFormOpen,
} from "@/store/appSlice";
import { ConsumerUnitInvoiceFilter } from "@/types/app";
import {
  EnergyBill,
  InvoiceDataGridRow,
  InvoicePayload,
  InvoicesPayload,
} from "@/types/consumerUnit";
import ConfirmDelete from "@/components/ConfirmDelete/ConfirmDelete";
import { parseNumberToMonth } from "@/utils/parseNumberToMonth";
import GenericInvoiceTable from "@/components/ConsumerUnit/Table/InvoiceTable";

const getMonthFromNumber = (
  month: number,
  year: number,
  shouldCapitalize?: boolean
) => {
  const date = new Date(year, month, 1);
  const monthFullName = format(date, "MMMM", { locale: ptBR });

  if (!shouldCapitalize) {
    return monthFullName;
  }

  return monthFullName.charAt(0).toUpperCase() + monthFullName.slice(1);
};

const getFilteredInvoices = (
  invoicesPayload: InvoicesPayload,
  activeFilter: ConsumerUnitInvoiceFilter
) => {
  if (activeFilter === "pending") {
    return Object.values(invoicesPayload)
      .flat()
      .filter(({ isEnergyBillPending }) => isEnergyBillPending);
  }

  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const currentMonth = currentDate.getMonth();

  return invoicesPayload[activeFilter].filter(
    (invoice) => invoice.month <= currentMonth || invoice.year < currentYear
  );
};

const formatConsumptionDemandToPtBrCurrency = (energyBill: EnergyBill) => {
  return {
    invoiceInReais:
      typeof energyBill.invoiceInReais == "number"
        ? formatToPtBrCurrency(energyBill.invoiceInReais, 2)
        : "-",
    offPeakConsumptionInKwh: formatNumberConditional(
      energyBill.offPeakConsumptionInKwh,
      2
    ),
    peakConsumptionInKwh: formatNumberConditional(
      energyBill.peakConsumptionInKwh
    ),
    offPeakMeasuredDemandInKw: formatNumberConditional(
      energyBill.offPeakMeasuredDemandInKw,
      2
    ),
    peakMeasuredDemandInKw: formatNumberConditional(
      energyBill.peakMeasuredDemandInKw,
      2
    ),
  };
};

const getDataGridRows = (
  invoicesPayload: InvoicePayload[],
  activeFilter: ConsumerUnitInvoiceFilter,
  isActive: boolean | undefined
): InvoiceDataGridRow[] => {
  return invoicesPayload
    .map(({ month, year, isEnergyBillPending, energyBill }) => ({
      ...energyBill,
      id: parseInt(`${year}${month >= 10 ? month : "0" + month}`),
      ...(energyBill && {
        energyBillId: energyBill.id,
        ...formatConsumptionDemandToPtBrCurrency(energyBill),
      }),
      month,
      year,
      isEnergyBillPending,
      activeFilter,
      isActive,
    }))
    .filter((row) => {
      if (
        row.isActive === false &&
        row.isEnergyBillPending === false &&
        row.invoiceInReais === undefined
      ) {
        return false;
      }
      if (!row.isActive && row.isEnergyBillPending) {
        return false;
      }
      return true;
    });
};

const ConsumerUnitInvoiceContentTable = () => {
  const dispatch = useDispatch();
  const consumerUnitId = useSelector(selectActiveConsumerUnitId);
  const [deleteEnergiBill] = useDeleteEnergiBillMutation();
  const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState(false);
  const [selectedBillenergyId, setSelectedEnergyBillId] = useState<number>(0);
  const [selectedMonth, setSelectedMonth] = useState<number>(0);
  const [selectedYear, setSelectedYear] = useState<number>(0);
  const [sortOrder, setSortOrder] = useState({ field: "month", direction: "asc" });

  const { data: consumerUnit } = useGetConsumerUnitQuery(
    consumerUnitId ?? skipToken
  );

  const { data: invoicesPayload } = useFetchInvoicesQuery(
    consumerUnitId ?? skipToken,
    {
      refetchOnMountOrArgChange: true,
    }
  );

  const activeFilter = useSelector(selectConsumerUnitInvoiceActiveFilter);
  const dataGridRows = useSelector(selectConsumerUnitInvoiceDataGridRows);

  const handleEditInvoiceFormOpen = (params: {
    month: number;
    year: number;
    id: number;
  }) => {
    const { month, year, id } = params;
    dispatch(setIsEnergyBillEdiFormOpen(true));
    dispatch(setEnergyBillEdiFormParams({ month, year, id }));
  };

  const handleDeleteInvoice = async () => {
    try {
      await deleteEnergiBill(selectedBillenergyId);
    } catch (error) {
      console.error("Failed to delete invoice:", error);
    }
  };

  const confirmDelete = () => {
    setIsDeleteDialogOpen(false);
    handleDeleteInvoice();
  };

  const cancelDelete = () => {
    setIsDeleteDialogOpen(false);
  };

  const columns = [
    {
      field: "month",
      header: "Mês",
      sortable: true,
      render: (item) => renderMonthCell(item),
    },
    {
      field: "isAtypical",
      header: <InsightsRounded />,
      render: (item) =>
        item.energyBillId === undefined
          ? ""
          : item.isAtypical
          ? <CancelRounded />
          : <CheckCircleOutlineRounded />,
    },
    {
      field: "peakConsumptionInKwh",
      header: "Ponta",
      align: "right",
      sortable: true,
      render: (item) => item.peakConsumptionInKwh,
    },
    {
      field: "offPeakConsumptionInKwh",
      header: "Fora Ponta",
      align: "right",
      sortable: true,
      render: (item) => item.offPeakConsumptionInKwh,
    },
    {
      field: "peakMeasuredDemandInKw",
      header: "Ponta",
      align: "right",
      sortable: true,
      render: (item) => item.peakMeasuredDemandInKw,
    },
    {
      field: "offPeakMeasuredDemandInKw",
      header: "Fora Ponta",
      align: "right",
      sortable: true,
      render: (item) => item.offPeakMeasuredDemandInKw,
    },
    {
      field: "invoiceInReais",
      header: "Valor (R$)",
      align: "right",
      sortable: true,
      render: (item) => item.invoiceInReais,
    },
    {
      field: "id",
      header: "",
      render: (item) => (
        <>
          <Tooltip title="Editar" arrow placement="top">
            <IconButton
              style={{ color: "#000000DE" }}
              onClick={() =>
                handleEditInvoiceFormOpen({
                  month: item.month,
                  year: item.year,
                  id: item.energyBillId,
                })
              }
            >
              <Edit />
            </IconButton>
          </Tooltip>
          <Tooltip title="Excluir" arrow placement="top">
            <IconButton
              style={{ color: "#000000DE" }}
              onClick={() => {
                setSelectedEnergyBillId(item.energyBillId);
                setSelectedMonth(item.month);
                setSelectedYear(item.year);
                setIsDeleteDialogOpen(true);
              }}
            >
              <Delete />
            </IconButton>
          </Tooltip>
        </>
      ),
    },
  ];

  useEffect(() => {
    if (!invoicesPayload || consumerUnit === undefined) {
      return;
    }

    const filteredInvoices = getFilteredInvoices(invoicesPayload, activeFilter);
    const dataGridRows = getDataGridRows(
      filteredInvoices,
      activeFilter,
      consumerUnit?.isActive
    );

    dispatch(setConsumerUnitInvoiceDataGridRows(dataGridRows));
  }, [activeFilter, invoicesPayload, consumerUnit, dispatch]);

  const handleOpenAddEnergyBillForm = useCallback(
    (month: number, year: number) => {
      dispatch(setIsEnergyBillCreateFormOpen(true));
      dispatch(setEnergyBillEdiFormParams({ month, year }));
    },
    [dispatch]
  );

  const renderMonthCell = useCallback(
    (invoiceRow: InvoiceDataGridRow) => {
      const {
        activeFilter,
        isEnergyBillPending,
        month,
        year,
        energyBillId,
        isActive,
      } = invoiceRow;

      const buttonLabel =
        "Lançar " +
        getMonthFromNumber(month, year) +
        `${activeFilter === "pending" ? "  " + year : ""}`;

      if (!isActive) {
        return getMonthFromNumber(month, year, true);
      }

      if (isEnergyBillPending) {
        return (
          <Button
            variant="contained"
            color="secondary"
            onClick={() => handleOpenAddEnergyBillForm(month, year)}
            startIcon={<WarningRounded />}
          >
            {buttonLabel}
          </Button>
        );
      }

      if (!energyBillId) {
        return (
          <Button
            variant="contained"
            onClick={() => handleOpenAddEnergyBillForm(month, year)}
          >
            {buttonLabel}
          </Button>
        );
      }

      return getMonthFromNumber(month, year, true);
    },
    [handleOpenAddEnergyBillForm]
  );

  const handleSort = (field) => {
    setSortOrder((prev) => ({
      field,
      direction: prev.direction === "asc" ? "desc" : "asc",
    }));
  };

  const sortedData = [...dataGridRows].sort((a, b) => {
    const valueA = a[sortOrder.field];
    const valueB = b[sortOrder.field];
    if (valueA < valueB) return sortOrder.direction === "asc" ? -1 : 1;
    if (valueA > valueB) return sortOrder.direction === "asc" ? 1 : -1;
    return 0;
  });

  return (
    <>
      <Grid container justifyContent="flex-end"></Grid>
      <GenericInvoiceTable
        columns={columns}
        data={sortedData}
        sortOrder={sortOrder}
        onSort={handleSort}
        hasRowWithErrorInCsv={undefined}
      />
      <ConfirmDelete
        title={`Apagar fatura de ${parseNumberToMonth(
          selectedMonth
        )} de ${selectedYear}?`}
        open={isDeleteDialogOpen}
        onConfirm={confirmDelete}
        onCancel={cancelDelete}
      />
    </>
  );
};

export default ConsumerUnitInvoiceContentTable;
