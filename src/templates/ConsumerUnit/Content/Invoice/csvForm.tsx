import React, { useEffect, useState } from "react";
import {
  Alert,
  AppBar,
  Box,
  Button,
  Container,
  Drawer,
  Grid,
  IconButton,
  Switch,
  Toolbar,
  Typography,
} from "@mui/material";
import {
  CloseRounded,
  ReportRounded,
} from "@mui/icons-material";
import { useDispatch, useSelector } from "react-redux";
import {
  selectActiveConsumerUnitId,
  selectIsCsvFormOpen,
  setIsCsvFormOpen,
  setIsErrorNotificationOpen,
  setIsSuccessNotificationOpen,
} from "@/store/appSlice";
import { useTheme } from "@mui/material/styles";
import { skipToken } from "@reduxjs/toolkit/dist/query";

import { useGetContractQuery, usePostMultipleInvoicesMutation } from "@/api";

import { format, parseISO } from "date-fns";
import { ptBR } from "date-fns/locale";
import GenericInvoiceTable from "@/components/ConsumerUnit/Table/InvoiceTable";
import { formatNumber } from "@/utils/number";

const formatDate = (dateString: string) => {
  try {
    const formattedDate = format(parseISO(dateString), "MMMM yyyy", {
      locale: ptBR,
    });
    return formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
  } catch {
    return dateString;
  }
};

export interface CsvData {
  consumerUnit: { value: string; error: boolean };
  date: {
    value: string;
    errors: [[number, string]] | null;
  };
  invoiceInReais: { value: string; errors: [[number, string]] | null };
  peakConsumptionInKwh: { value: string; errors: [[number, string]] | null };
  offPeakConsumptionInKwh: { value: string; errors: [[number, string]] | null };
  peakMeasuredDemandInKw: { value: string; errors: [[number, string]] | null };
  offPeakMeasuredDemandInKw: {
    value: string;
    errors: [[number, string]] | null;
  };
}

interface CsvFormProps {
  csvData: CsvData[];
}

const CsvForm: React.FC<CsvFormProps> = ({ csvData }) => {
  const dispatch = useDispatch();
  const isCsvFormOpen = useSelector(selectIsCsvFormOpen);
  const theme = useTheme();
  const consumerUnitId = useSelector(selectActiveConsumerUnitId);
  const { data: contractData } = useGetContractQuery(
    consumerUnitId ?? skipToken
  );
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");
  const [sortColumn, setSortColumn] = useState<string>("date");
  const [selectedRows, setSelectedRows] = useState<CsvData[]>([]);
  const [postMultipleInvoice, { isLoading }] =
    usePostMultipleInvoicesMutation();
  const [hasErrorInCsv, setHasErrorInCsv] = useState(false);
  const [isVisible, setIsVisible] = useState(isCsvFormOpen);

  const transformSelectedRows = (rows: CsvData[]) => {
    return rows.map((row) => ({
      consumerUnit: row.consumerUnit.value,
      date: row.date.value,
      invoiceInReais: parseFloat(row.invoiceInReais.value),
      peakConsumptionInKwh: parseFloat(row.peakConsumptionInKwh.value),
      offPeakConsumptionInKwh: parseFloat(row.offPeakConsumptionInKwh.value),
      peakMeasuredDemandInKw: parseFloat(row.peakMeasuredDemandInKw.value),
      offPeakMeasuredDemandInKw: parseFloat(
        row.offPeakMeasuredDemandInKw.value
      ),
    }));
  };

  const handleCloseDrawer = () => {
    dispatch(setIsCsvFormOpen(false));
  };

  const handleSort = (property: string) => {
    const isAsc = sortColumn === property && sortOrder === "asc";
    console.log(isVisible);
    setSortOrder(isAsc ? "desc" : "asc");
    setSortColumn(property);
  };
  const handleSwitchChange = (item: CsvData) => {
    setSelectedRows((prevSelectedRows) => {
      if (prevSelectedRows.includes(item)) {
        return prevSelectedRows.filter((row) => row !== item);
      } else {
        return [...prevSelectedRows, item];
      }
    });
  };

  const handleSubmitDrawer = async (event: React.FormEvent) => {
    event.preventDefault();
    console.log("Dados selecionados para envio:", selectedRows);

    const payload = {
      consumerUnit: consumerUnitId?.toString(),
      contract: contractData?.id.toString(),
      energyBills: transformSelectedRows(selectedRows),
    };

    const handleNotification = (isSuccess: boolean) => {
      if (isSuccess) {
        dispatch(
          setIsSuccessNotificationOpen({
            isOpen: true,
            text: messages.validation.invoice.addSuccess,
          })
        );
      } else {
        dispatch(
          setIsErrorNotificationOpen({
            isOpen: true,
            text: messages.validation.invoice.addError,
          })
        );
      }
    };

    try {
      const response = await postMultipleInvoice(payload).unwrap();
      console.log("Success:", response);
      handleNotification(true);
      // Optionally, close the drawer on success
      // handleCloseDrawer();
      handleCloseWithTransition();
    } catch (err) {
      handleNotification(false);
      console.error("Error:", err);
    }
  };

  const sortedData = [...csvData].sort((a, b) => {
    const aValue = a[sortColumn].value;
    const bValue = b[sortColumn].value;
  
    if (sortOrder === "asc") {
      return aValue < bValue ? -1 : aValue > bValue ? 1 : 0;
    } else {
      return aValue > bValue ? -1 : aValue < bValue ? 1 : 0;
    }
  });

  useEffect(() => {
    const initialSelected = csvData.filter(
      (item) => !hasRowWithErrorInCsv(item)
    );
    const hasErrorInCsv = csvData.every(hasRowWithErrorInCsv);

    setSelectedRows(initialSelected);
    setHasErrorInCsv(hasErrorInCsv);
  }, [csvData]);

  const [drawerStyle, setDrawerStyle] = useState({
    transform: "translateY(100%)",
    transition: "transform 0.2s ease-out",
  });

  useEffect(() => {
    if (isCsvFormOpen) {
      setIsVisible(true);
      setDrawerStyle({ transform: "translateY(0)", transition: "transform 0.2s ease-out" });
    } else {
      setDrawerStyle({ transform: "translateY(100%)", transition: "transform 0.2s ease-out" });
      setTimeout(() => setIsVisible(false), 500); // Aguarda a transição antes de ocultar
    }
  }, [isCsvFormOpen]);

  const hasRowWithErrorInCsv = (item: CsvData) => {
    return (
      item.date.errors ||
      item.peakConsumptionInKwh.errors ||
      item.offPeakConsumptionInKwh.errors ||
      item.peakMeasuredDemandInKw.errors ||
      item.offPeakMeasuredDemandInKw.errors ||
      item.invoiceInReais.errors
    );
  };

  const columns = [
    {
      field: "include",
      header: "Incluir",
      render: (item) => (
        <Box display="flex" justifyContent="center" alignItems="center">
          {hasRowWithErrorInCsv(item) ? (
            <ReportRounded style={{ color: theme.palette.error.main }} />
          ) : (
            <Switch
              checked={selectedRows.includes(item)}
              onChange={() => handleSwitchChange(item)}
            />
          )}
        </Box>
      ),
    },
    {
      field: "date",
      header: "Data",
      sortable: true,
      render: (item) => formatDate(item.date.value),
    },
    {
      field: "peakConsumptionInKwh",
      header: "Ponta",
      align: "right",
      render: (item) => formatNumber(item.peakConsumptionInKwh.value),
    },
    {
      field: "offPeakConsumptionInKwh",
      header: "Fora Ponta",
      align: "right",
      render: (item) => formatNumber(item.offPeakConsumptionInKwh.value),
    },
    {
      field: "peakMeasuredDemandInKw",
      header: "Ponta",
      align: "right",
      render: (item) =>
        item.peakMeasuredDemandInKw.value
          ? formatNumber(item.peakMeasuredDemandInKw.value)
          : "",
    },
    {
      field: "offPeakMeasuredDemandInKw",
      header: "Fora Ponta",
      align: "right",
      render: (item) => formatNumber(item.offPeakMeasuredDemandInKw.value),
    },
    {
      field: "invoiceInReais",
      header: "Valor (R$)",
      align: "right",
      render: (item) =>
        item.invoiceInReais.value
          ? formatNumber(item.invoiceInReais.value)
          : "",
    },
  ];

  return (
    <Drawer
      open={isCsvFormOpen}
      anchor="bottom"
      PaperProps={{ sx: { height: "100%", ...drawerStyle } }}
      onClose={handleCloseDrawer}
    >
      <AppBar position="sticky">
        <Toolbar>
          <Container maxWidth="lg">
            <Box display="flex" alignItems="center">
              <IconButton
                edge="start"
                style={{ color: "#000000DE" }}
                aria-label="Fechar formulário"
                onClick={handleCloseWithTransition}
              >
                <CloseRounded />
              </IconButton>

              <Box ml={3}>
                <Typography variant="h6">Importar planilha</Typography>
              </Box>
            </Box>
          </Container>
        </Toolbar>
      </AppBar>

      <Container maxWidth="lg">
        <Box mt={3} mb={6} component="form" onSubmit={handleSubmitDrawer}>
          <Box mt={4}>
            <Box p={2}>
            <GenericInvoiceTable
              columns={columns}
              data={sortedData}
              sortOrder={sortOrder}
              onSort={handleSort}
              hasRowWithErrorInCsv={hasRowWithErrorInCsv}
            />
            </Box>
          </Box>

          <Box display="flex" alignItems="center" justifyContent="center">
            <Box
              mt={4}
              display="flex"
              flexDirection="column"
              alignItems="self-start"
            >
              {hasErrorInCsv ? (
                <Alert
                  variant="filled"
                  severity="error"
                  style={{ marginBottom: "16px" }}
                >
                  Corrija os erros na planilha e importe-a novamente
                </Alert>
              ) : (
                <Alert
                  variant="filled"
                  severity="warning"
                  style={{ marginBottom: "8px" }}
                >
                  Apenas meses selecionados como “Incluir” serão gravados
                </Alert>
              )}
              <Grid item xs={12} style={{ display: "flex" }}>
                <Button
                  onClick={handleSubmitDrawer}
                  variant="contained"
                  color="primary"
                  size="large"
                  style={{ marginRight: "16px", width: "100px" }}
                  disabled={isLoading || selectedRows.length <= 0}
                >
                  Gravar
                </Button>
                <Button
                  variant="outlined"
                  onClick={handleCloseWithTransition}
                  size="large"
                  style={{ width: "100px" }}
                >
                  <Typography pl={3} pr={3}>
                    Cancelar
                  </Typography>
                </Button>
              </Grid>
            </Box>
          </Box>
        </Box>
      </Container>
    </Drawer>
  );
};

export default CsvForm;
