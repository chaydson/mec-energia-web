import messages from "./messages";

export const getVoltageBySubgroup = (subgroup: string) => {
  switch (subgroup) {
    case "AS":
      return messages.voltage.AS;
    case "A4":
      return messages.voltage.A4;
    case "A3a":
      return messages.voltage.A3a;
    case "A3":
      return messages.voltage.A3;
    case "A2":
      return messages.voltage.A2;
    case "A1":
      return messages.voltage.A1;
    default:
      return messages.voltage.NA;
  }
};
