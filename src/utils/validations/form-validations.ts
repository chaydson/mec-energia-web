import { format, isAfter, isBefore, isFuture, isValid } from "date-fns";
import { Control, FieldValues } from "react-hook-form";
import messages from "../messages";

export const MAX_EMAIL_LENGTH = 254;
export const MAX_PASSWORD_LENGTH = 128;
export const MIN_PASSWORD_LENGTH = 8;
export const MAX_ACRONYM_LENGTH = 50;
export const MAX_INSTITUTION_NAME_LENGTH = 100;
export const MAX_FLOAT_VALUE_9_2 = 9999999.99;
export const MAX_FLOAT_VALUE_7_2 = 99999.99;

export const isValidEmail = (email: string) => {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(email)) return messages.validation.email.invalid;
  return true;
};

export const isValidDate = (date: Date | null) => {
  if (!date || !isValid(date)) {
    return "Insira uma data válida no formato dd/mm/aaaa";
  }

  const dateToCompare = new Date(date.getTime());
  dateToCompare.setHours(0, 1, 0, 0);

  if (isFuture(dateToCompare)) {
    return "Datas futuras não são permitidas";
  }

  if (!isAfter(dateToCompare, new Date("2010"))) {
    return "Datas antes de 2010 não são permitidas";
  }

  return true;
};

export const validateComparedDates = (
  date: Date | null,
  dateToCompare?: Date | null
) => {
  if (!dateToCompare) return true;

  if (date && isBefore(date, dateToCompare)) {
    return `Datas anteriores a ${format(
      dateToCompare,
      "dd/MM/yyyy"
    )} não serão permitidas`;
  }

  return true;
};

/**
 * @param fields Todos os campos do formulário que podem retornar algum erro do backend.
 *   @name fields[key].errorMessage Mensagem de erro exibida no frontend, abaixo do respectivo input.
 *   @name fields[key].expectedErrorMessage Mensagem de erro vinda do backend
 * @param control Hook do React para controle de formulário
 * @param responseErrorData Objeto com a resposta de erro vindo do backend
 * @returns void
 */
export const validateFieldsFromHttpResponse = <T extends FieldValues>(
  fields: {
    [key: string]: {
      errorMessage: string;
      expectedErrorMessage: string;
    };
  },
  control: Control<T, unknown>,
  responseErrorData: Record<string, unknown>
): void => {
  if (!responseErrorData) return;

  for (const field of Object.keys(fields)) {
    const errorMessages = responseErrorData[field];

    if (
      Array.isArray(errorMessages) &&
      errorMessages[0] === fields[field].expectedErrorMessage
    ) {
      control.setError(field, {
        message: fields[field].errorMessage,
        type: "validate",
      });
    }
  }

  // Percorre todas as chaves do objeto de erro para buscar possíveis nonFieldErrors
  for (const key of Object.keys(responseErrorData)) {
    const errorGroup = responseErrorData[key];

    if (
      typeof errorGroup === "object" &&
      errorGroup !== null &&
      "nonFieldErrors" in errorGroup &&
      Array.isArray(errorGroup.nonFieldErrors)
    ) {
      const nonFieldErrors = errorGroup.nonFieldErrors as string[];

      for (const field of Object.keys(fields)) {
        const { expectedErrorMessage, errorMessage } = fields[field];

        if (nonFieldErrors.includes(expectedErrorMessage)) {
          control.setError(field, {
            message: errorMessage,
            type: "validate",
          });
        }
      }
    }
  }
};

export const hasEnoughCaracteresLength = (
  value: string,
  maxLength = 50,
  minLength = 3
) => {
  if (!value) return;
  if (value.length < minLength || value.length > maxLength)
    return `Insira entre ${minLength} e ${maxLength} caracteres`;
  return true;
};

export const hasConsecutiveSpaces = (value: string) => {
  if (/\s{2,}/.test(value)) return "Não são permitidos espaços consecutivos";
  return true;
};

export const hasNoSpecialCharacters = (value: string) => {
  const regex = /^[a-zA-Z\s\u00C0-\u017F]*$/;
  if (!regex.test(value))
    return "Insira somente letras, sem números ou caracteres especiais";
  return true;
};
