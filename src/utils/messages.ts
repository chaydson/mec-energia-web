const messages = {
  head: {
    withOutTitle: " | MEPA - Monitoramento de Energia em Plataforma Aberta",
    withTitle: "MEPA - Monitoramento de Energia em Plataforma Aberta",
  },
  voltage: {
    AS: "Inferior a 2,3 kV",
    A4: "2,3 kV a 25 kV",
    A3a: "30 kV a 44 kV",
    A3: "69 kV",
    A2: "88 kV a 138 kV",
    A1: "230 kV ou superior",
    NA: "N/A",
  },
  energyBills: {
    upToDate: "Em dia",
    oneBillPending: "1 fatura pendente",
    severalBillsPending: "Faturas pendentes",
    addSuccess: "Fatura lançada com sucesso!",
    addError: "Erro ao lançar fatura!",
    editSuccess: "Fatura modificada com sucesso!",
    editError: "Erro ao modificar fatura!",
  },
  validation: {
    email: {
      invalid: "Insira um e-mail válido",
      alreadyExists: "Já existe uma pessoa cadastrada com este e-mail",
    },
    date: {
      invalid: "Insira uma data válida no formato dd/mm/aaaa.",
      before2010NotAllowed: "Insira uma data a partir de 2010",
      futureNotAllowed:
        "Insira uma data anterior ou igual à data atual no formato dd/mm/aaaa.",
      endDateInvalid: "Insira uma data posterior à data de início",
    },
    name: {
      tooShort: "Insira ao menos 3 caracteres",
      tooLong: "O máximo permitido é 45 caracteres",
      noSpecialCharacters:
        "Insira somente letras, sem números ou caracteres especiais",
      noConsecutiveSpaces: "Não são permitidos espaços consecutivos",
    },
    Institution: {
      addSuccess: "Instituição adicionada com sucesso!",
      addError: "Erro ao adicionar instituição.",
      editSuccess: "Instituição editada com sucesso!",
      editError: "Erro ao editar instituição.",
    },
    Distributor: {
      addSuccess: "Distribuidora adicionada com sucesso!",
      addError: "Erro ao adicionar distribuidora.",
      editSuccess: "Distribuidora modificada com sucesso!",
      editError: "Erro ao editar distribuidora.",
      deleteSuccess: "Distribuidora excluída com sucesso!",
      deleteError: "Erro ao excluir distribuidora.",
    },
    renewContract: {
      renewSuccess: "Contrato renovado com sucesso!",
      renewError: "Erro ao renovar contrato",
    },
    contract: {
      valueGreaterThanZero: "Insira um valor maior que 0,00",
      valueGreaterThanZeroReal: "Insira um valor maior que R$ 0,00",
      valueGreaterThanZeroInt: "Insira um valor maior que 0",
      valueGreaterThanThirty: "Insira um valor maior ou igual a 30",
    },
    consumerUnit: {
      addSuccess: "Unidade consumidora adicionada com sucesso!",
      addError:
        "Erro ao adicionar unidade consumidora. Verifique se já existe uma unidade com  nome ou código",
      editSuccess: "Unidade consumidora modificada com sucesso!",
      editError:
        "Erro ao editar unidade consumidora. Verifique se já existe uma unidade com o mesmo nome ou código",
    },
    password: {
      incorrect: "Senha incorreta",
      editSuccess: "Senha alterada com sucesso!",
      restartedSuccess: "Senha reiniciada com sucesso!",
      tooShort: "A senha deve ter no mínimo 8 caracteres",
      equal: 'Insira uma senha idêntica à "Nova senha"',
    },
    textLength: {
      between3And25: "Insira entre 3 e 25 caracteres",
      between3And50: "Insira entre 3 e 50 caracteres",
    },
    required: {
      field: "Preencha este campo",
      university: "Selecione alguma universidade",
      mandatory: "Campo obrigatório",
    },
    person: {
      addSuccess: "Pessoa adicionada com sucesso!",
      editSuccess: "Pessoa editada com sucesso!",
      editError: "Erro ao editar pessoa.",
    },
    tariff: {
      addSuccess: "Tarifas adicionadas com sucesso!",
      addError: "Erro ao adicionar tarifas!",
      editSuccess: "Tarifas atualizadas com sucesso",
      editError: "Erro ao editar tarifa",
    },
    invoice: {
      addSuccess: "Faturas lançadas com sucesso",
      addError: "Erro ao lançar faturas",
      alreadyExists: "Já existe uma fatura lançada neste mês",
    },
    cnpj: {
      notValid: "Insira um CNPJ válido com 14 dígitos",
    },
  },
};

export default messages;