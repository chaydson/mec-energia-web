export const formatNumber = (numberString: string) => {
  const number = parseFloat(numberString);
  if (isNaN(number)) return numberString;
  return number.toLocaleString("pt-BR", {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  });
};