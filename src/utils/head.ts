import messages from "./messages";

export const getHeadTitle = (title?: string) => {
  if (!title) {
    return messages.head.withTitle;
  }

  return title.trim() + messages.head.withOutTitle;
};
