import React from "react";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { ArrowDownward, ArrowUpward } from "@mui/icons-material";
import { formatNumber } from "@/utils/number";
import { CsvData } from "@/templates/ConsumerUnit/Content/Invoice/csvForm";
import { InvoiceDataGridRow } from "@/types/consumerUnit";
import { ReactNode } from "react";

interface Column<T> {
  field: keyof T;
  header: ReactNode;
  sortable?: boolean;
  align?: "left" | "center" | "right";
  render?: (item: T) => ReactNode;
}

interface GenericTableProps<T> {
  columns: Column<T>[];
  data?: InvoiceDataGridRow[] | CsvData[];
  sortOrder: {field: string;direction: string;} | string;
  onSort: ((field: string) => void) | (() => void);
  hasRowWithErrorInCsv?: (item: CsvData) => [[number, string]] | null;
}

const GenericInvoiceTable: React.FC<GenericTableProps> = ({
  columns,
  data,
  sortOrder,
  onSort,
  hasRowWithErrorInCsv,
}) => {
  const theme = useTheme();

  return (
    <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
      <Table>
      <TableHead>
          <TableRow
            style={{
              backgroundColor: theme.palette.background.default,
              color: "#000",
              border: "none",
            }}
          >
            <TableCell
              colSpan={1}
              style={{
                backgroundColor: "transparent",
                borderBottom: "none",
                padding: "8px",
              }}
            ></TableCell>
            <TableCell
              colSpan={1}
              style={{
                backgroundColor: "transparent",
                border: "none",
                borderBottom: "none",
                padding: "8px",
              }}
            ></TableCell>
            <TableCell
              colSpan={2}
              align="center"
              style={{
                backgroundColor: "#0A5C6714",
                color: "#000",
                border: "none",
                borderBottom: "none",
                padding: "8px",
              }}
            >
              Consumo medido (kWh)
            </TableCell>
            <TableCell
              colSpan={2}
              align="center"
              style={{
                backgroundColor: "#0A5C6714",
                color: "#000",
                border: "none",
                borderBottom: "none",
                padding: "8px",
              }}
            >
              Demanda medida (kW)
            </TableCell>
            <TableCell
              colSpan={1}
              style={{
                backgroundColor: "transparent",
                border: "none",
                borderBottom: "none",
                padding: "8px",
              }}
            ></TableCell>
          </TableRow>
            
          <TableRow
            style={{
              backgroundColor: theme.palette.background.default,
              color: "#000",
              border: "none",
            }}
          >
            {columns.map((column, index) => (
              <TableCell
                key={index}
                colSpan={column.colSpan || 1}
                align={column.align || "left"}
                style={{
                  backgroundColor: column.backgroundColor || "transparent",
                  color: column.color || "#000",
                  border: "none",
                  borderBottom: "none",
                  padding: "8px",
                }}
              >
                {column.label}
              </TableCell>
            ))}
          </TableRow>
          <TableRow>
            {columns.map((column, index) => (
              <TableCell
                key={index}
                onClick={column.sortable ? () => onSort(column.field) : undefined}
                style={{
                  cursor: column.sortable ? "pointer" : "default",
                  whiteSpace: "nowrap",
                }}
              >
                {column.header}
                {column.sortable &&
                  (sortOrder.field === column.field &&
                  sortOrder.direction === "asc" ? (
                    <ArrowUpward fontSize="small" />
                  ) : (
                    <ArrowDownward fontSize="small" />
                  ))}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>

        <TableBody>
          {data.map((item, index) => {
            const hasError = hasRowWithErrorInCsv ? hasRowWithErrorInCsv(item) : null;
            const errorMessages = new Set();
            Object.keys(item).forEach((key) => {
              if (item[key].errors) {
                item[key].errors.forEach(([, msg]) => {
                  errorMessages.add("- " + msg);
                });
              }
            });

            return (
              <React.Fragment key={index}>
                <TableRow
                  style={{
                    backgroundColor:
                      index % 2 === 0
                        ? "#FFFFFF"
                        : theme.palette.background.default,
                  }}
                >
                  {columns.map((column, colIndex) => (
                    <TableCell
                      key={colIndex}
                      rowSpan={hasError && colIndex === 0 ? 2 : 1}
                      style={{
                        backgroundColor: item[column.field]?.errors
                          ? theme.palette.error.main
                          : "inherit",
                        color: item[column.field]?.errors ? "#FFFFFF" : "inherit",
                        borderBottom: hasError
                          ? "none"
                          : "1px solid #e0e0e0",
                        ...column.style,
                      }}
                    >
                      {column.render
                        ? column.render(item)
                        : item[column.field]?.value
                        ? formatNumber(item[column.field].value)
                        : ""}
                    </TableCell>
                  ))}
                </TableRow>
                {hasError && (
                  <TableRow
                    style={{
                      backgroundColor:
                        index % 2 === 0
                          ? "#FFFFFF"
                          : theme.palette.background.default,
                    }}
                  >
                    <TableCell
                      colSpan={columns.length}
                      style={{
                        color: theme.palette.error.main,
                        textAlign: "left",
                        paddingLeft: "0px",
                      }}
                    >
                      {Array.from(errorMessages).map((msg, idx) => (
                        <div key={idx}>{msg}</div>
                      ))}
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default GenericInvoiceTable;