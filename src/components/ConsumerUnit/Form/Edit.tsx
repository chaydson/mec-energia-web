import { Fragment, useCallback, useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { NumericFormat } from "react-number-format";
import {
  Alert,
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  FormGroup,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";

import {
  selectIsConsumerUnitEditFormOpen,
  setIsConsumerUnitEditFormOpen,
  selectActiveConsumerUnitId,
  setIsSuccessNotificationOpen,
} from "@/store/appSlice";
import {
  EditConsumerUnitForm,
  EditConsumerUnitRequestPayload,
} from "@/types/consumerUnit";
import FormWarningDialog from "@/components/ConsumerUnit/Form/WarningDialog";
import {
  useEditConsumerUnitMutation,
  useGetConsumerUnitQuery,
  useGetContractQuery,
  useGetDistributorsQuery,
} from "@/api";
import { useSession } from "next-auth/react";
import { DistributorPropsTariffs } from "@/types/distributor";
import DistributorCreateFormDialog from "@/components/Distributor/Form/CreateForm";
import { skipToken } from "@reduxjs/toolkit/dist/query";
import { sendFormattedDate } from "@/utils/date";
import FormDrawerV2 from "@/components/Form/DrawerV2";
import FlashOnIcon from "@mui/icons-material/FlashOn";
import FormFieldError from "@/components/FormFieldError";
import { minimumDemand } from "@/utils/tariff";
import { isValidDate, MAX_FLOAT_VALUE_7_2, MAX_FLOAT_VALUE_9_2, validateFieldsFromHttpResponse } from "@/utils/validations/form-validations";
import messages from "@/utils/messages";

const defaultValues: EditConsumerUnitForm = {
  isActive: true,
  name: "",
  code: "",
  distributor: "",
  startDate: new Date(),
  subgroup: "",
  tariffFlag: "B",
  peakContractedDemandInKw: "",
  offPeakContractedDemandInKw: "",
  totalInstalledPower: null,
  shouldShowInstalledPower: true,
};

const ConsumerUnitEditForm = () => {
  const dispatch = useDispatch();
  const isEditFormOpen = useSelector(selectIsConsumerUnitEditFormOpen);
  const activeConsumerUnit = useSelector(selectActiveConsumerUnitId);
  const [shouldShowDistributorFormDialog, setShouldShowDistributorFormDialog] =
    useState(false);
  const [shouldShowCancelDialog, setShouldShowCancelDialog] = useState(false);
  const [shouldShowGreenDemand, setShouldShowGreenDemand] = useState(true);

  const { data: session } = useSession();
  const { data: distributorList } = useGetDistributorsQuery(
    session?.user?.universityId || skipToken
  );
  const [currentDistributor, setCurrentDistributor] = useState();

  const handleDistributorChange = useCallback((event) => {
    const selectedDistributor = event.id || event.target.value;

    setCurrentDistributor(selectedDistributor);
    setValue("distributor", selectedDistributor);
    clearErrors("distributor");
  }, []);

  const mappedDistributorList = distributorList?.map((distributor) => {
    const idCopy = distributor.id || distributor.value;
    const valueCopy = distributor.value || distributor.id;

    return {
      ...distributor,
      id: idCopy,
      value: valueCopy,
    };
  });

  const sortedDistributorList = useMemo(() => {
    return mappedDistributorList
      ?.slice()
      .sort((a, b) => a.name.localeCompare(b.name));
  }, [isEditFormOpen, distributorList]);

  const { data: contract } = useGetContractQuery(
    activeConsumerUnit || skipToken
  );
  const { data: consumerUnit, refetch: refetchConsumerUnit } =
    useGetConsumerUnitQuery(activeConsumerUnit || skipToken);
  const [
    editConsumerUnit,
    { isSuccess, isLoading, reset: resetMutation },
  ] = useEditConsumerUnitMutation();

  const form = useForm({ mode: "all", defaultValues });

  const {
    control,
    reset,
    handleSubmit,
    watch,
    setValue,
    clearErrors,
    formState: { isDirty, errors },
  } = form;

  const tariffFlag = watch("tariffFlag");
  const subgroup = watch("subgroup");
  const peakContractedDemandInKw = watch("peakContractedDemandInKw");
  const offPeakContractedDemandInKw = watch("offPeakContractedDemandInKw");
  const isActive = watch("isActive");
  const shouldShowInstalledPower = watch("shouldShowInstalledPower");

  useEffect(
    () => {
      if (isEditFormOpen && consumerUnit && contract) {
        const fetchData = async () => {
          try {
            const { data: consumerUnit } = await refetchConsumerUnit();

            if (!consumerUnit || !contract) {
              return;
            }

            setValue("name", consumerUnit?.name ?? "");
            setValue("isActive", true);
            setValue("code", consumerUnit?.code ?? "");
            setValue("distributor", contract?.distributor);
            setCurrentDistributor(contract?.distributor);
            setValue("subgroup", contract?.subgroup);
            setValue(
              "shouldShowInstalledPower",
              consumerUnit?.totalInstalledPower != null
            );
            setValue("totalInstalledPower", consumerUnit?.totalInstalledPower);

            if (contract?.subgroup === "A3") {
              setShouldShowGreenDemand(false);
            } else if (
              contract?.subgroup === "A2"
            ) {
              setShouldShowGreenDemand(false);
            } else {
              setShouldShowGreenDemand(true);
            }
            setValue(
              "peakContractedDemandInKw",
              contract?.peakContractedDemandInKw
            );
            setValue(
              "offPeakContractedDemandInKw",
              contract?.offPeakContractedDemandInKw
            );

            const currentDate = new Date(contract?.startDate);
            currentDate.setDate(currentDate.getDate() + 1);
            setValue("startDate", currentDate);
          } catch (err) {
            console.error("Failed to refetch:", err);
          }
        };

        // Garante que o refetch não seja executado antes do fetch
        if (isEditFormOpen) {
          fetchData();
        }
      }
    },
    [isEditFormOpen, consumerUnit, contract, setValue],
    refetchConsumerUnit
  );

  useEffect(() => {
    setValue("isActive", isActive);
    if (subgroup === undefined) {
      setValue("subgroup", "");
      return;
    }
    if (peakContractedDemandInKw === undefined) {
      setValue("peakContractedDemandInKw", "");
      return;
    }
    if (offPeakContractedDemandInKw === undefined) {
      setValue("offPeakContractedDemandInKw", "");
      return;
    }
  }, [
    isActive,
    offPeakContractedDemandInKw,
    peakContractedDemandInKw,
    setValue,
    subgroup,
  ]);

  useEffect(() => {
    // Verifica se shouldShowGreenDemand é false
    if (!shouldShowGreenDemand) {
      // Atualiza o estado tariffFlag para "B" (azul)
      setValue("tariffFlag", "B");
    }
  }, [shouldShowGreenDemand, setValue]);

  useEffect(() => {
    setValue("tariffFlag", contract?.tariffFlag ?? "B");
  }, [isEditFormOpen, contract?.tariffFlag, setValue]);

  const hasEnoughCaracteresLength = (
    value: EditConsumerUnitForm["code"] | EditConsumerUnitForm["name"]
  ) => {
    if (value.length < 3) return messages.validation.name.tooShort;
    return true;
  };

  const handleCloseDialog = useCallback(() => {
    setShouldShowCancelDialog(false);
  }, []);

  const handleDiscardForm = useCallback(() => {
    handleCloseDialog();
    reset();
    dispatch(setIsConsumerUnitEditFormOpen(false));
  }, [dispatch, handleCloseDialog, reset]);

  const handleCancelEdition = useCallback(() => {
    if (isDirty) {
      setShouldShowCancelDialog(true);
      return;
    }

    handleDiscardForm();
  }, [handleDiscardForm, isDirty]);

  const onSubmitHandler: SubmitHandler<EditConsumerUnitForm> = useCallback(
    async (data) => {
      if (data.tariffFlag === "G") {
        data.offPeakContractedDemandInKw = data.peakContractedDemandInKw;
      }
      const body: EditConsumerUnitRequestPayload = {
        consumerUnit: {
          consumerUnitId: activeConsumerUnit as number,
          name: data.name,
          code: data.code,
          isActive: data.isActive,
          university: session?.user.universityId || 0,
          totalInstalledPower: !data.shouldShowInstalledPower
            ? null
            : data.totalInstalledPower,
        },
        contract: {
          contractId: contract?.id as number,
          startDate: data.startDate ? sendFormattedDate(data.startDate) : "",
          tariffFlag: data.tariffFlag,
          peakContractedDemandInKw: data.peakContractedDemandInKw as number,
          offPeakContractedDemandInKw:
            data.offPeakContractedDemandInKw as number,
          subgroup: data.subgroup,
          distributor: data.distributor as number,
        },
      };
      const response = await editConsumerUnit(body);
      if (response.error && response.error.data) {
        validateFieldsFromHttpResponse<EditConsumerUnitForm>(
          {
            name: {
              errorMessage: "Já existe uma unidade consumidora com este nome.",
              expectedErrorMessage: "The fields university, name must make a unique set.",
            },
            code: {
              errorMessage: "Já existe uma unidade consumidora com este número.",
              expectedErrorMessage: "The fields university, code must make a unique set.",
            },
          },
          control,
          response.error.data
        );
      }
    },
    [
      activeConsumerUnit,
      contract?.id,
      editConsumerUnit,
      session?.user.universityId,
    ]
  );

  const handleNotification = () => {
    if (isSuccess) {
      dispatch(
        setIsSuccessNotificationOpen({
          isOpen: true,
          text: messages.validation.consumerUnit.editSuccess,
        })
      );
      reset();
      resetMutation();
      dispatch(setIsConsumerUnitEditFormOpen(false));
    }
  };

  useEffect(() => {
    handleNotification();
  }, [isSuccess]);

  const handleCloseDistributorFormDialog = () => {
    setShouldShowDistributorFormDialog(false);
  };

  const handleNumericInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    onChange: (value: string) => void
  ) => {
    const numericValue = e.target.value.replace(/\D/g, "");
    onChange(numericValue);
  };

  const ConsumerUnitSection = useCallback(
    () => (
      <>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h5">Unidade Consumidora</Typography>
          </Grid>

          <Grid item xs={12}>
            <Controller
              control={control}
              name="name"
              rules={{
                required: messages.validation.required.field,
                validate: (value) => hasEnoughCaracteresLength(value, 100),
              }}
              render={({
                field: { onChange, onBlur, value, ref },
                fieldState: { error },
              }) => (
                <TextField
                  ref={ref}
                  value={value}
                  label="Nome *"
                  placeholder="Ex.: Campus Gama, Biblioteca, Faculdade de Medicina"
                  error={Boolean(error)}
                  helperText={FormFieldError(error?.message)}
                  fullWidth
                  onChange={onChange}
                  onBlur={onBlur}
                  inputProps={{ maxLength: 100 }}
                />
              )}
            />
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Controller
            name="isActive"
            control={control}
            render={({ field: { onChange, value } }) => (
              <FormGroup>
                <Box
                  display="flex"
                  justifyContent="flex-start"
                  alignItems="center"
                  marginTop={0}
                  style={{ marginTop: 0, paddingTop: 0 }}
                >
                  <FlashOnIcon color="primary" />
                  {consumerUnit && (
                    <FormControlLabel
                      label="Unidade ativa"
                      labelPlacement="start"
                      sx={{ margin: 0.5, fontSize: '16px', marginTop: "1px" ,letterSpacing: 0.15, lineHeight: '24px' }}
                      control={
                        <Box>
                          <Switch
                            value={value}
                            defaultChecked={consumerUnit?.isActive}
                            onChange={onChange}
                          />
                        </Box>
                      }
                    />
                  )}
                </Box>

                <FormHelperText sx={{marginLeft: 3.5, width: '407px',letterSpacing: 0.6, lineHeight: '16px', fontWeight: 400, textAlign:'left'}}>
                  <span>
                    Só unidades ativas geram recomendações e recebem faturas.
                    Não é possível excluir unidades, apenas desativá-las.
                  </span>
                </FormHelperText>
              </FormGroup>
            )}
          />
        </Grid>
      </>
    ),
    [consumerUnit?.isActive, control]
  );

  const ContractSection = useCallback(
    () => (
      <>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h5">Contrato</Typography>
          </Grid>

          <Grid item xs={12}>
            <Alert severity="warning" variant="standard">
              Modifique o contrato apenas em caso de erro de digitação. Para
              alterações legais ou novo contrato, use a opção{" "}
              <strong>Renovar</strong> na tela anterior.
            </Alert>
          </Grid>

          <Grid item xs={12}>
            <Controller
              control={control}
              name="code"
              rules={{
                required: messages.validation.required.field,
                validate: (value) => hasEnoughCaracteresLength(value, 30),
              }}
              render={({
                field: { onChange, onBlur, value, ref },
                fieldState: { error },
              }) => (
                <TextField
                  ref={ref}
                  value={value}
                  label="Número da unidade *"
                  placeholder="Número da Unidade Consumidora conforme a fatura"
                  error={Boolean(error)}
                  helperText={FormFieldError(
                    error?.message,
                    "Nº ou código da Unidade Consumidora conforme a fatura"
                  )}
                  fullWidth
                  onChange={(e) => handleNumericInputChange(e, onChange)}
                  onBlur={onBlur}
                  inputProps={{ maxLength: 30 }}
                />
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              control={control}
              name="distributor"
              rules={{ required: messages.validation.required.field }}
              render={({ field: { value, onBlur, ref }, fieldState: { error } }) => (
                <FormControl
                  sx={{ minWidth: "200px", maxWidth: "100%" }}
                  error={!!error}
                >
                  <InputLabel>Distribuidora *</InputLabel>

                  <Select
                    ref={ref}
                    value={value}
                    label="Distribuidora *"
                    MenuProps={{
                      anchorOrigin: {
                        vertical: "bottom",
                        horizontal: "left",
                      },
                      transformOrigin: {
                        vertical: "top",
                        horizontal: "left",
                      },
                    }}
                    onChange={handleDistributorChange}
                    onBlur={onBlur}
                  >
                    {sortedDistributorList?.map(
                      (distributor: DistributorPropsTariffs) => {
                        return (
                          <MenuItem key={distributor.id} value={distributor.id}>
                            {distributor.name}
                          </MenuItem>
                        );
                      }
                    )}
                    <MenuItem>
                      <Button
                        onClick={() => setShouldShowDistributorFormDialog(true)}
                      >
                        Adicionar
                      </Button>
                    </MenuItem>
                  </Select>

                  <FormHelperText>
                    {FormFieldError(error?.message)}
                  </FormHelperText>
                </FormControl>
              )}
            />
          </Grid>

          <Grid item xs={12}>
            <Controller
              control={control}
              name="startDate"
              rules={{
                required: messages.validation.required.field,
                validate: isValidDate,
              }}
              render={({
                field: { value, onChange },
                fieldState: { error },
              }) => (
                <DatePicker
                  value={value}
                  label="Início da vigência *"
                  views={["day", "month", "year"]}
                  minDate={new Date("2010")}
                  disableFuture
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      inputProps={{
                        ...params.inputProps,
                        placeholder: "dd/mm/aaaa",
                      }}
                      helperText={FormFieldError(error?.message)}
                      error={!!error}
                    />
                  )}
                  onChange={onChange}
                />
              )}
            />
          </Grid>

          <Grid item xs={8} sm={6}>
            <Controller
              control={control}
              name={"subgroup"}
              rules={{ required: messages.validation.required.field }}
              render={({
                field: { onChange, onBlur, value, ref },
                fieldState: { error },
              }) => (
                <FormControl
                  sx={{ minWidth: "200px", maxWidth: "100%" }}
                  error={!!error}
                  style={{ marginTop: '20px' }}
                >
                  <InputLabel>Subgrupo e tensão contratada *</InputLabel>

                  <Select
                    defaultValue="A4"
                    ref={ref}
                    value={value}
                    label="Subgrupo e tensão contratada *"
                    autoWidth
                    MenuProps={{
                      anchorOrigin: {
                        vertical: "bottom",
                        horizontal: "left",
                      },
                      transformOrigin: {
                        vertical: "top",
                        horizontal: "left",
                      },
                    }}
                    onChange={onChange}
                    onBlur={onBlur}
                  >
                    <MenuItem value={"AS"}>
                      Subgrupo AS: inferior a 2,3 kV
                    </MenuItem>
                    <MenuItem value={"A4"}>
                      Subgrupo A4: 2,3 kV a 25 kV
                    </MenuItem>
                    <MenuItem value={"A3a"}>
                      Subgrupo A3a: 30 kV a 44 kV
                    </MenuItem>
                    <MenuItem value={"A3"}>
                      Subgrupo A3: 69 kV
                    </MenuItem>
                    <MenuItem value={"A2"}>
                      Subgrupo A2: 88 kV a 138 kV
                    </MenuItem>
                    <MenuItem value={"A1"}>
                      Subgrupo A1: 230 kV ou superior
                    </MenuItem>
                  </Select>

                  <FormHelperText>{FormFieldError(error?.message)}</FormHelperText>
                </FormControl>
              )}
            />
          </Grid>
        </Grid>
      </>
    ),
    [
      control,
      currentDistributor,
      sortedDistributorList,
      distributorList,
      setValue,
      handleDistributorChange,
    ]
  );

  const ContractedDemand = useCallback(
    () => (
      <>
        <Grid item xs={12}>
          <Typography variant="h5">Demanda contratada</Typography>
        </Grid>
        <Grid item xs={12}>
          <Controller
            control={control}
            name="tariffFlag"
            rules={{ required: messages.validation.required.field }}
            render={({ field: { value, onChange }, fieldState: { error } }) => (
              <FormControl error={!!error}>
                <FormLabel>Modalidade tarifária *</FormLabel>

                <RadioGroup value={value} onChange={onChange}>
                  <Box
                    display={"flex"}
                    justifyContent="flex-start"
                    alignItems="center"
                  >
                    <FormControlLabel
                      value="G"
                      control={<Radio />}
                      label="Verde"
                      disabled={!shouldShowGreenDemand}
                    />
                    <FormHelperText>(Demanda única)</FormHelperText>
                  </Box>
                  <Box
                    display={"flex"}
                    justifyContent="flex-start"
                    alignItems="center"
                  >
                    <FormControlLabel
                      value="B"
                      control={<Radio />}
                      label="Azul"
                    />
                    <FormHelperText>
                      (Demanda de ponta e fora ponta)
                    </FormHelperText>
                  </Box>
                </RadioGroup>

                <FormHelperText>{error?.message ?? " "}</FormHelperText>
              </FormControl>
            )}
          />
        </Grid>
        {tariffFlag === "G" ? (
          <Grid item xs={12} container spacing={2}>
            <Grid item xs={5}>
              <Controller
                control={control}
                name="peakContractedDemandInKw"
                rules={{
                  required: messages.validation.required.field,
                  min: minimumDemand,
                }}
                render={({
                  field: { onChange, onBlur, value },
                  fieldState: { error },
                }) => (
                  <NumericFormat
                    value={value}
                    customInput={TextField}
                    label="Demanda *"
                    fullWidth
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">kW</InputAdornment>
                      ),
                    }}
                    type="text"
                    allowNegative={false}
                    isAllowed={({ floatValue }) =>
                      !floatValue || floatValue <= MAX_FLOAT_VALUE_9_2
                    }
                    decimalScale={2}
                    decimalSeparator=","
                    thousandSeparator={"."}
                    error={Boolean(error)}
                    helperText={FormFieldError(error?.message)}
                    onValueChange={(values) => onChange(values.floatValue)}
                    onBlur={onBlur}
                  />
                )}
              />
            </Grid>
          </Grid>
        ) : (
          <>
            <Grid item xs={12} container spacing={2}>

              <Grid item xs={5}>
                <Controller
                  control={control}
                  name="peakContractedDemandInKw"
                  rules={{
                    required: messages.validation.required.field,
                    min: minimumDemand,
                  }}
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <NumericFormat
                      value={value}
                      customInput={TextField}
                      label="Dem. Ponta *"
                      fullWidth
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">kW</InputAdornment>
                        ),
                      }}
                      type="text"
                      allowNegative={false}
                      isAllowed={({ floatValue }) =>
                        !floatValue || floatValue <= MAX_FLOAT_VALUE_9_2
                      }
                      decimalScale={2}
                      decimalSeparator=","
                      thousandSeparator={"."}
                      error={Boolean(error)}
                      helperText={error?.message ?? " "}
                      onValueChange={(values) => onChange(values.floatValue)}
                      onBlur={onBlur}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={5}>
                <Controller
                  control={control}
                  name="offPeakContractedDemandInKw"
                  rules={{
                    required: messages.validation.required.field,
                    min: minimumDemand,
                  }}
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <NumericFormat
                      value={value}
                      customInput={TextField}
                      label="Dem. Fora Pta *"
                      fullWidth
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">kW</InputAdornment>
                        ),
                      }}
                      type="text"
                      allowNegative={false}
                      isAllowed={({ floatValue }) =>
                        !floatValue || floatValue <= MAX_FLOAT_VALUE_9_2
                      }
                      decimalScale={2}
                      decimalSeparator=","
                      thousandSeparator={"."}
                      error={Boolean(error)}
                      helperText={error?.message ?? " "}
                      onValueChange={(values) => onChange(values.floatValue)}
                      onBlur={onBlur}
                    />
                  )}
                />
              </Grid>
            </Grid>
            {!shouldShowGreenDemand && (
              <Typography variant="body2" sx={{ px: 2 }}>
                O valor de tensão contratada inserido é compatível apenas com a
                modalidade azul
              </Typography>
            )}
          </>
        )}
      </>
    ),
    [control, tariffFlag, shouldShowGreenDemand, sortedDistributorList]
  );

  const InstalledPower = useCallback(
    () => (
      <>
        <Grid container spacing={2}>
          <Grid
            item
            xs={12}
            display="flex"
            flexDirection={"row"}
            justifyContent={"begin"}
            alignItems={"center"}
          >
            <Typography variant="h5">Geração de energia</Typography>
            <Controller
              name="shouldShowInstalledPower"
              control={control}
              render={({ field: { onChange, value } }) => (
                <FormControl>
                  <FormControlLabel
                    sx={{ marginLeft: 0.5 }}
                    control={
                      <Switch
                        value={value}
                        defaultChecked={shouldShowInstalledPower}
                        onChange={onChange}
                      />
                    }
                  />
                </FormControl>
              )}
            />
          </Grid>
          {shouldShowInstalledPower ? (
            <>
              <Grid item xs={12}>
                <Alert severity="info" variant="standard">
                  Insira o valor total da potência de geração instalada na
                  Unidade Consumidora. Some a potência de todas as plantas
                  fotovoltaicas instaladas, se houver mais de uma.
                </Alert>
              </Grid>

              <Grid item xs={5.3}>
                <Controller
                  control={control}
                  name="totalInstalledPower"
                  rules={{
                    required: messages.validation.required.field,
                    min: {
                      value: 0.01,
                      message:
                        messages.validation.contract.valueGreaterThanZeroReal,
                    },
                  }}
                  render={({
                    field: { onChange, onBlur, value },
                    fieldState: { error },
                  }) => (
                    <NumericFormat
                      value={value}
                      customInput={TextField}
                      label="Potência instalada *"
                      fullWidth
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">kW</InputAdornment>
                        ),
                      }}
                      type="text"
                      allowNegative={false}
                      isAllowed={({ floatValue }) =>
                        !floatValue || floatValue <= MAX_FLOAT_VALUE_7_2
                      }
                      decimalScale={2}
                      decimalSeparator=","
                      thousandSeparator={"."}
                      error={Boolean(error)}
                      helperText={FormFieldError(error?.message)}
                      onValueChange={(values) => onChange(values.floatValue)}
                      onBlur={onBlur}
                    />
                  )}
                />
              </Grid>
            </>
          ) : null}
        </Grid>
      </>
    ),
    [control, shouldShowInstalledPower]
  );

  return (
    <Fragment>
      <FormDrawerV2
        open={isEditFormOpen}
        title={"Editar Unidade Consumidora"}
        errorsLength={Object.keys(errors).length}
        isLoading={isLoading}
        handleCloseDrawer={handleCancelEdition}
        handleSubmitDrawer={handleSubmit(onSubmitHandler)}
        sections={[
          <ConsumerUnitSection key={0} />,
          <ContractSection key={1} />,
          <ContractedDemand key={2} />,
          <InstalledPower key={3} />,
        ]}
        header={<></>}
      />

      <FormWarningDialog
        open={shouldShowCancelDialog}
        onClose={handleCloseDialog}
        onDiscard={handleDiscardForm}
        entity={"unidade consumidora"}
        type="update"
      />

      <DistributorCreateFormDialog
        open={shouldShowDistributorFormDialog}
        onClose={handleCloseDistributorFormDialog}
        handleDistributorChange={handleDistributorChange}
      />
    </Fragment>
  );
};

export default ConsumerUnitEditForm;
