import { Box, IconButton, Typography } from "@mui/material";
import StarRoundedIcon from "@mui/icons-material/StarRounded";
import StarOutlineRoundedIcon from "@mui/icons-material/StarOutlineRounded";

import { CardProps } from "@/types/app";
import CardWrapper from "@/components/Card/Wrapper";

import TodayIcon from "@mui/icons-material/Today";
import FlashOffIcon from "@mui/icons-material/FlashOff";
import WarningIcon from "@mui/icons-material/Warning";
import FactoryIcon from "@mui/icons-material/Factory";

const Card = ({
  name,
  selected,
  dense,
  variant,
  isFavorite,
  isDashboard,
  action,
  actionIcon,
  isCurrentEnergyBillFilled,
  pendingEnergyBillsNumber,
  pendingTariffsCount,
  onClick,
  onActionIconClick,
  onFavoriteButtonClick,
}: CardProps) => {
  const isActive = variant !== "disabled";
  const isDisabled = variant === "disabled";
  const isWarning = variant === "warning";
  const canFavorite = isFavorite !== undefined;
  const shouldShowFavoriteIconButton = canFavorite && isActive;

  const shouldShowActionIconButton = actionIcon && !dense && isActive;

  const getBackgroundIcon = () => {
    if (pendingTariffsCount) {
      return (
        <FactoryIcon
          sx={{
            position: "absolute",
            fontSize: "46px",
            color: "rgba(0, 0, 0, 0.1)",
            top: "22%",
            left: "88%",
            transform: "translate(-50%, -50%)",
            pointerEvents: "none",
          }}
        />
      );
    }
    if (!isActive) {
      return (
        <FlashOffIcon
          sx={{
            position: "absolute",
            fontSize: "46px",
            color: "rgba(0, 0, 0, 0.1)",
            top: "22%",
            left: "88%",
            transform: "translate(-50%, -50%)",
            pointerEvents: "none",
          }}
        />
      );
    }

    if (pendingEnergyBillsNumber) {
      return (
        <WarningIcon
          sx={{
            position: "absolute",
            fontSize: "46px",
            color: "rgba(0, 0, 0, 0.1)",
            top: "22%",
            left: "88%",
            transform: "translate(-50%, -50%)",
            pointerEvents: "none",
          }}
        />
      );
    }

    if (isCurrentEnergyBillFilled) {
      return (
        <TodayIcon
          sx={{
            position: "absolute",
            fontSize: "46px",
            color: "rgba(0, 0, 0, 0.1)",
            top: "22%",
            left: "88%",
            transform: "translate(-50%, -50%)",
            pointerEvents: "none",
          }}
        />
      );
    }

    return null; 
  };

  return (
    <CardWrapper
      selected={selected}
      dense={dense}
      variant={variant}
      onClick={onClick}
    >
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        sx={{
          justifyContent:
            !shouldShowFavoriteIconButton && isDashboard
              ? "flex-end"
              : "space-between",
        }}
      >
        {getBackgroundIcon()}
        {shouldShowFavoriteIconButton && (
          <Box display="flex" minHeight="30px">
            <IconButton
              edge="start"
              sx={{
                color: isWarning ? "black" : "primary.main",
                mt: -1.5,
              }}
              onClick={onFavoriteButtonClick}
            >
              {isFavorite ? <StarRoundedIcon /> : <StarOutlineRoundedIcon />}
            </IconButton>
          </Box>
        )}

        <Box display="flex" flexDirection="column" justifyContent="end">
          <Box textOverflow="ellipsis" display="flex" mb={1.5}>
            <Typography
              sx={{
                ...(!dense && {
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  display: "-webkit-box",
                  WebkitLineClamp: "2",
                  WebkitBoxOrient: "vertical",
                }),
              }}
              title={name}
              position="relative"
              fontWeight={400}
              fontSize="20px"
              lineHeight="24px"
              minWidth={150}
            >
              {name}
            </Typography>
          </Box>

          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="end"
            {...(!dense && { minHeight: "30.75px" })}
          >
            {isDisabled ? (
              <Typography color="text.secondary">Desativada</Typography>
            ) : (
              action
            )}

            {shouldShowActionIconButton && (
              <Box alignSelf="center" m={-1}>
                <IconButton
                  style={{ color: "#000000DE" }}
                  onClick={onActionIconClick}
                >
                  {actionIcon}
                </IconButton>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
    </CardWrapper>
  );
};

export default Card;
