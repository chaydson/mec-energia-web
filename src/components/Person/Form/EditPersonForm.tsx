import React, { Fragment, useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  selectActivePersonId,
  selectIsPersonEditFormOpen,
  setIsErrorNotificationOpen,
  setIsPersonEditFormOpen,
  setIsSuccessNotificationOpen,
} from "../../../store/appSlice";

import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { Grid, TextField, Typography } from "@mui/material";
import FormWarningDialog from "../../ConsumerUnit/Form/WarningDialog";
import {
  EditPersonForm,
  EditPersonRequestPayload,
  UserRole,
} from "@/types/person";
import {
  useEditPersonMutation,
  useGetPersonQuery,
  useGetUniversityPersonQuery,
} from "@/api";
import { hasEnoughCaracteresLength, isValidEmail, MAX_EMAIL_LENGTH, validateFieldsFromHttpResponse } from "@/utils/validations/form-validations";
import { skipToken } from "@reduxjs/toolkit/query";
import FormDrawerV2 from "@/components/Form/DrawerV2";
import FormFieldError from "@/components/FormFieldError";
import messages from "@/utils/messages";

const defaultValues: EditPersonForm = {
  email: "",
  firstName: "",
  lastName: "",
  university: null,
  type: UserRole.UNIVERSITY_USER,
};

const EditPersonComponent = () => {
  const dispatch = useDispatch();
  const isEditFormOpen = useSelector(selectIsPersonEditFormOpen);
  const [shouldShowCancelDialog, setShouldShowCancelDialog] = useState(false);
  const currentPersonId = useSelector(selectActivePersonId);
  const { data: currentPerson, refetch: refetchPerson } = useGetPersonQuery(
    currentPersonId || skipToken
  );
  const { data: universityPerson } = useGetUniversityPersonQuery(
    currentPersonId || skipToken
  );
  const [editPerson, { isError, isSuccess, isLoading, reset: resetMutation }] =
    useEditPersonMutation();
  const form = useForm({ mode: "all", defaultValues });
  const {
    control,
    reset,
    handleSubmit,
    setValue,
    formState: { isDirty, errors },
  } = form;
  const handleCancelEdition = () => {
    if (isDirty) {
      setShouldShowCancelDialog(true);
      return;
    }
    handleDiscardForm();
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data: currentPerson } = await refetchPerson();
        if (!currentPerson) return;

        setValue("firstName", currentPerson.firstName);
        setValue("lastName", currentPerson.lastName);
        setValue("email", currentPerson.email);
      } catch (err) {
        console.error("Failed to refetch:", err);
      }
    };

    // Garante que o refetch não seja executado antes do fetch
    if (isEditFormOpen) {
      fetchData();
    }
  }, [currentPerson, isEditFormOpen, setValue]);

  const handleDiscardForm = useCallback(() => {
    handleCloseDialog();
    reset();
    dispatch(setIsPersonEditFormOpen(false));
  }, [dispatch, reset]);

  const handleCloseDialog = () => {
    setShouldShowCancelDialog(false);
  };

  const onSubmitHandler: SubmitHandler<EditPersonForm> = async (data) => {
    const { email, firstName, lastName } = data;

    if (!currentPerson) return;
    if (!universityPerson) return;
    const body: EditPersonRequestPayload = {
      email,
      firstName,
      lastName,
      type: currentPerson.type,
      university: universityPerson.university,
      id: currentPerson?.id,
    };

    const response = await editPerson(body);

    if (response.error && response.error.data) {
      validateFieldsFromHttpResponse<EditPersonForm>(
        {
          firstName: {
            errorMessage: messages.validation.textLength.between3And25,
            expectedErrorMessage: "Ensure this field has no more than 25 characters.",
          },
          lastName: {
            errorMessage: messages.validation.textLength.between3And50,
            expectedErrorMessage: "Ensure this field has no more than 50 characters.",
          },
          email: {
            errorMessage: messages.validation.email.alreadyExists,
            expectedErrorMessage: "User with this Email already exists.",
          },
        },
        control,
        response.error.data
      );
    }
  };

  //Notificações
  const handleNotification = useCallback(() => {
    if (isSuccess) {
      dispatch(
        setIsSuccessNotificationOpen({
          isOpen: true,
          text: messages.validation.person.editSuccess,
        })
      );
      reset();
      resetMutation();
      dispatch(setIsPersonEditFormOpen(false));
    } else if (isError) {
      dispatch(
        setIsErrorNotificationOpen({
          isOpen: true,
          text: messages.validation.person.editError,
        })
      );
      resetMutation();
    }
  }, [dispatch, isError, isSuccess, reset, resetMutation]);

  useEffect(() => {
    handleNotification();
  }, [handleNotification, isSuccess, isError]);

  const cardTitleStyles: CardTitleStyle = {
    marginBottom: "15px",
  };

  const PersonalInformationSection = useCallback(
    () => (
      <>
        <Grid item xs={12}>
          <Typography variant="h5" style={cardTitleStyles}>
            Informações pessoais
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Controller
            control={control}
            name="firstName"
            rules={{
              required: messages.validation.required.field,
              validate: (value) => hasEnoughCaracteresLength(value, 25),
            }}
            render={({
              field: { onChange, onBlur, value, ref },
              fieldState: { error },
            }) => (
              <TextField
                ref={ref}
                value={value}
                label="Nome *"
                error={Boolean(error)}
                helperText={FormFieldError(error?.message)}
                fullWidth
                onChange={onChange}
                onBlur={onBlur}
                inputProps={{ maxLength: 25 }}
              />
            )}
          />
        </Grid>

        <Grid item xs={12} mt={0.3}>
          <Controller
            control={control}
            name="lastName"
            rules={{
              required: messages.validation.required.field,
              validate: (value) => hasEnoughCaracteresLength(value, 50),
            }}
            render={({
              field: { onChange, onBlur, value, ref },
              fieldState: { error },
            }) => (
              <TextField
                ref={ref}
                value={value}
                label="Sobrenome *"
                error={Boolean(error)}
                helperText={FormFieldError(error?.message)}
                fullWidth
                onChange={onChange}
                onBlur={onBlur}
                inputProps={{ maxLength: 50 }}
              />
            )}
          />
        </Grid>

        <Grid item xs={12} mt={0.3}>
          <Controller
            control={control}
            name="email"
            rules={{
              required: messages.validation.required.field,
              validate: {
                validEmail: isValidEmail,
                hasEnoughCaracteresLength: (value) => hasEnoughCaracteresLength(value, MAX_EMAIL_LENGTH),
              }
            }}
            render={({
              field: { onChange, onBlur, value, ref },
              fieldState: { error },
            }) => (
              <TextField
                ref={ref}
                value={value}
                label="E-mail institucional *"
                placeholder="Ex.: voce@universidade.br"
                error={Boolean(error)}
                helperText={FormFieldError(error?.message)}
                fullWidth
                onChange={onChange}
                onBlur={onBlur}
                inputProps={{ maxLength: MAX_EMAIL_LENGTH }}
              />
            )}
          />
        </Grid>
      </>
    ),
    [control]
  );

  return (
    <Fragment>
      <FormDrawerV2
        errorsLength={Object.keys(errors).length}
        open={isEditFormOpen}
        handleCloseDrawer={handleCancelEdition}
        handleSubmitDrawer={handleSubmit(onSubmitHandler)}
        isLoading={isLoading}
        title="Editar Pessoa"
        header={<></>}
        sections={[<PersonalInformationSection key={0} />]}
      />

      <FormWarningDialog
        open={shouldShowCancelDialog}
        entity={"registro"}
        onClose={handleCloseDialog}
        onDiscard={handleDiscardForm}
        type="update"
      />
    </Fragment>
  );
};

export default EditPersonComponent;
