import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSession } from "next-auth/react";
import {
  selectIsPersonCreateFormOpen,
  setIsPersonCreateFormOpen,
  setIsSuccessNotificationOpen,
} from "../../../store/appSlice";

import { Controller, SubmitHandler, useForm } from "react-hook-form";
import {
  Autocomplete,
  Box,
  FormControl,
  FormControlLabel,
  FormHelperText,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@mui/material";
import FormWarningDialog from "../../ConsumerUnit/Form/WarningDialog";

import {
  CreatePersonForm,
  CreatePersonRequestPayload,
  UserRole,
} from "@/types/person";
import { useCreatePersonMutation, useGetAllInstitutionQuery } from "@/api";
import {
  hasEnoughCaracteresLength,
  isValidEmail,
  MAX_EMAIL_LENGTH,
  validateFieldsFromHttpResponse
} from "@/utils/validations/form-validations";
import { FormInfoAlert } from "@/components/Form/FormInfoAlert";
import FormDrawerV2 from "@/components/Form/DrawerV2";
import FormFieldError from "@/components/FormFieldError";
import messages from "@/utils/messages";

const defaultFormValues: CreatePersonForm = {
  email: "",
  firstName: "",
  lastName: "",
  university: null,
  type: UserRole.UNIVERSITY_USER,
};

const CreatePersonComponent = () => {
  const dispatch = useDispatch();
  const isCreateFormOpen = useSelector(selectIsPersonCreateFormOpen);
  const [shouldShowCancelDialog, setShouldShowCancelDialog] = useState(false);
  const { data: institutions } = useGetAllInstitutionQuery();
  const [
    createPerson,
    { isError, isSuccess, isLoading, reset: resetMutation },
  ] = useCreatePersonMutation();
  const { data: session } = useSession();
  const form = useForm({ mode: "all", defaultValues: defaultFormValues });
  const {
    control,
    reset,
    handleSubmit,
    formState: { isDirty, errors },
  } = form;
  const handleCancelEdition = () => {
    if (isDirty) {
      setShouldShowCancelDialog(true);
      return;
    }
    handleDiscardForm();
  };

  const cardTitleStyles: CardTitleStyle = {
    marginBottom: "15px",
  };

  const institutionsOptions = useMemo(() => {
    return institutions?.map((institution) => ({
      label: institution.acronym ? institution.acronym + ' - ' + institution.name : institution.name,
      id: institution.id,
    })).sort((a, b) => a.label.localeCompare(b.label));
  }, [institutions]);

  const handleDiscardForm = useCallback(() => {
    handleCloseDialog();
    reset();
    dispatch(setIsPersonCreateFormOpen(false));
  }, [dispatch, reset]);

  const handleCloseDialog = () => {
    setShouldShowCancelDialog(false);
  };

  const onSubmitHandler: SubmitHandler<CreatePersonForm> = async (data) => {
    const { email, firstName, lastName, type, university } = data;

    // Verifica se o usuário não é um super usuário e define o ID da universidade com base na sessão do usuário
    const universityId =
      session?.user?.type !== UserRole.SUPER_USER
        ? session?.user?.universityId ?? 0
        : university?.id ?? 0;

    const body: CreatePersonRequestPayload = {
      email,
      firstName,
      lastName,
      type,
      university: universityId,
    };

    const response = await createPerson(body);

    if (response.error && response.error.data) {
      validateFieldsFromHttpResponse<CreatePersonForm>(
        {
          firstName: {
            errorMessage: messages.validation.textLength.between3And25,
            expectedErrorMessage: "Ensure this field has no more than 25 characters.",
          },
          lastName: {
            errorMessage: messages.validation.textLength.between3And50,
            expectedErrorMessage: "Ensure this field has no more than 50 characters.",
          },
          email: {
            errorMessage: messages.validation.email.alreadyExists,
            expectedErrorMessage: "User with this Email already exists.",
          },
        },
        control,
        response.error.data
      );
    }
  };

  //Notificações
  const handleNotification = useCallback(() => {
    if (isSuccess) {
      dispatch(
        setIsSuccessNotificationOpen({
          isOpen: true,
          text: messages.validation.person.addSuccess,
        })
      );
      reset();
      resetMutation();
      dispatch(setIsPersonCreateFormOpen(false));
    }
  }, [dispatch, isError, isSuccess, reset, resetMutation]);

  useEffect(() => {
    handleNotification();
  }, [handleNotification, isSuccess, isError]);

  const PersonalInformationSection = useCallback(
    () => (
      <>
        <Grid item xs={12}>
          <Typography variant="h5" style={cardTitleStyles}>
            Informações pessoais
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Controller
            control={control}
            name="firstName"
            rules={{
              required: messages.validation.required.field,
              validate: (value) => hasEnoughCaracteresLength(value, 25),
            }}
            render={({
              field: { onChange, onBlur, value, ref },
              fieldState: { error },
            }) => (
              <TextField
                style={{ width: "220px" }}
                ref={ref}
                value={value}
                label="Nome *"
                error={Boolean(error)}
                helperText={FormFieldError(error?.message)}
                onChange={onChange}
                onBlur={onBlur}
                inputProps={{ maxLength: 25 }}
              />
            )}
          />
        </Grid>

        <Grid item xs={12}>
          <Controller
            control={control}
            name="lastName"
            rules={{
              required: messages.validation.required.field,
              validate: (value) => hasEnoughCaracteresLength(value),
            }}
            render={({
              field: { onChange, onBlur, value, ref },
              fieldState: { error },
            }) => (
              <TextField
                ref={ref}
                value={value}
                label="Sobrenome *"
                error={Boolean(error)}
                helperText={FormFieldError(error?.message)}
                fullWidth
                onChange={onChange}
                onBlur={onBlur}
                inputProps={{ maxLength: 50 }}
              />
            )}
          />
        </Grid>

        <Grid item xs={12}>
          <Controller
            control={control}
            name="email"
            rules={{
              required: messages.validation.required.field,
              validate: {
                validEmail: isValidEmail,
                hasEnoughCaracteresLength: (value) => hasEnoughCaracteresLength(value, MAX_EMAIL_LENGTH),
              }
            }}
            render={({
              field: { onChange, onBlur, value, ref },
              fieldState: { error },
            }) => (
              <TextField
                ref={ref}
                value={value}
                label="E-mail institucional *"
                placeholder="Ex.: voce@universidade.br"
                error={Boolean(error)}
                helperText={FormFieldError(error?.message)}
                fullWidth
                onChange={onChange}
                onBlur={onBlur}
                inputProps={{ maxLength: MAX_EMAIL_LENGTH }}
              />
            )}
          />
        </Grid>

        {session?.user?.type === UserRole.SUPER_USER && (
          <Grid item xs={12}>
            <Controller
              control={control}
              name={"university"}
              rules={{ required: messages.validation.required.university }}
              render={({
                field: { onChange, onBlur, value },
                fieldState: { error },
              }) => (
                <>
                  <Autocomplete
                    id="university-select"
                    options={institutionsOptions || []}
                    getOptionLabel={(option) => option.label}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        label="Instituição *"
                        placeholder="Selecione uma instituição"
                        error={!!error}
                      />
                    )}
                    value={value}
                    onBlur={onBlur}
                    onChange={(_, data) => {
                      onChange(data);
                      return data;
                    }}
                  />
                  {errors.university !== undefined && (
                    <Typography
                      mt={0.4}
                      ml={2}
                      sx={{ color: "error.main", fontSize: 13 }}
                    >
                      {errors.university.message}
                    </Typography>
                  )}
                </>
              )}
            />
          </Grid>
        )}
      </>
    ),
    [control, errors.university, institutionsOptions]
  );

  const PerfilSection = useCallback(
    () => (
      <>
        <Grid item xs={12}>
          <Typography variant="h5">Perfil</Typography>
        </Grid>

        <Grid item xs={8}>
          <Controller
            control={control}
            name="type"
            rules={{ required: messages.validation.required.field }}
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <FormControl error={!!error}>
                <RadioGroup value={value} onChange={onChange}>
                  <Box
                    display={"flex"}
                    flexDirection="column"
                    justifyContent="flex-start"
                    alignItems="self-start"
                  >
                    <FormControlLabel
                      value="university_user"
                      control={<Radio />}
                      label="Operacional"
                    />
                    <FormHelperText>
                      Acesso às tarefas básicas do sistema como: gerenciar
                      unidades consumidoras e distribuidoras, lançar faturas e
                      tarifas, além de gerar recomendações.
                    </FormHelperText>
                  </Box>
                  <Box
                    display={"flex"}
                    flexDirection="column"
                    justifyContent="flex-start"
                    alignItems="self-start"
                  >
                    <FormControlLabel
                      value="university_admin"
                      control={<Radio />}
                      label="Gestão"
                    />
                    <FormHelperText>
                      Permite gerenciar o perfil das outras pessoas que usam o
                      sistema, além das tarefas operacionais.
                    </FormHelperText>
                  </Box>
                </RadioGroup>
                <FormInfoAlert infoText="A pessoa receberá um e-mail com instruções para gerar uma senha e realizar o primeiro acesso ao sistema." />
                <FormHelperText>{error?.message ?? " "}</FormHelperText>
              </FormControl>
            )}
          />
        </Grid>
      </>
    ),
    [control]
  );

  return (
    <Fragment>
      <FormDrawerV2
        errorsLength={Object.keys(errors).length}
        open={isCreateFormOpen}
        handleCloseDrawer={handleCancelEdition}
        handleSubmitDrawer={handleSubmit(onSubmitHandler)}
        isLoading={isLoading}
        title="Adicionar Pessoa"
        header={<></>}
        sections={[
          <PersonalInformationSection key={0} />,
          <PerfilSection key={1} />,
        ]}
      />

      <FormWarningDialog
        open={shouldShowCancelDialog}
        entity={"registro"}
        onClose={handleCloseDialog}
        onDiscard={handleDiscardForm}
        type="create"
      />
    </Fragment>
  );
};

export default CreatePersonComponent;
