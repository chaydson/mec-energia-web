import { render, screen } from '@testing-library/react';
import { RecommendationCard } from '../../../src/templates/Analysis/RecommendationCard';
import '@testing-library/jest-dom';

describe('RecommendationCard', () => {
  const mockRecommendation = {
    id: 1,
    consumerUnitId: 1,
    date: '2025-01-01',
    currentContract: {
      id: 1,
      startDate: '2025-01-01',
      endDate: '2025-12-31',
      peakContractedDemandInKw: 100,
      offPeakContractedDemandInKw: 100,
      supplyVoltage: 'A4',
      tariffFlag: 'VERDE',
      subgroup: 'A4',
    },
    recommendedContract: {
      id: 2,
      startDate: '2025-01-01',
      endDate: '2025-12-31',
      peakContractedDemandInKw: 150,
      offPeakContractedDemandInKw: 150,
      supplyVoltage: 'A4',
      tariffFlag: 'AZUL',
      subgroup: 'A4',
    },
    shouldRenewContract: true,
    annualSavings: 12000,
    idealPeakDemand: 150,
    idealOffPeakDemand: 150,
  };

  it('renders error state when hasErrors is true', () => {
    render(
      <RecommendationCard
        hasErrors={true}
        recommendation={undefined}
        minimumPercentageForContractRenovation={10}
      />
    );
    expect(screen.getByText('Recomendação')).toBeInTheDocument();
    expect(screen.getByText('Indisponível')).toBeInTheDocument();
  });

  it('renders recommendation when contract should be renewed', () => {
    render(
      <RecommendationCard
        hasErrors={false}
        recommendation={mockRecommendation}
        minimumPercentageForContractRenovation={10}
      />
    );
    expect(screen.getByText('Ajustar contrato')).toBeInTheDocument();
    expect(screen.getByText('Modalidade tarifária')).toBeInTheDocument();
  });

  it('renders with annual savings', () => {
    const { container } = render(
      <RecommendationCard
        hasErrors={false}
        recommendation={mockRecommendation}
        minimumPercentageForContractRenovation={10}
      />
    );

    // Verifica se o componente foi renderizado
    expect(container.firstChild).toBeInTheDocument();

    // Verifica se o texto de economia está presente de alguma forma
    const cardContent = container.querySelector('.MuiCardContent-root');
    expect(cardContent).toHaveTextContent('RecomendaçãoAjustar contratoModalidade tarifária AtualPropostoDemanda contratada - carga Atual0 kWProposto0 kW');
  });

  it('renders when contract renewal is not recommended', () => {
    const recommendationWithoutRenewal = {
      ...mockRecommendation,
      shouldRenewContract: false
    };

    render(
      <RecommendationCard
        hasErrors={false}
        recommendation={recommendationWithoutRenewal}
        minimumPercentageForContractRenovation={10}
      />
    );
    expect(screen.getByText('Manter contrato')).toBeInTheDocument();
  });
});
