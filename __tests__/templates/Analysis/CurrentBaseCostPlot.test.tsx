import { render } from "@testing-library/react";
import { CurrentBaseCostPlot } from "@/templates/Analysis/CurrentBaseCostPlot";
import { Chart } from "react-chartjs-2";
import { ContractCostsPlot } from "@/types/recommendation";

// Mock Chart.js
jest.mock("react-chartjs-2", () => ({
  Chart: jest.fn(() => null),
}));

describe("CurrentBaseCostPlot", () => {
  const mockDates = [["Jan/2023"], ["Fev/2023"]];
  const mockGraphData: ContractCostsPlot = {
    demandCostInReais: [200, null],
    consumptionCostInReais: [150, 180],
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders correctly with datasets", () => {
    render(<CurrentBaseCostPlot dates={mockDates} currentContractCostsPlot={mockGraphData} />);
    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    expect(chartCall.data.datasets).toHaveLength(2);
    expect(chartCall.data.datasets[0]).toEqual({
      label: "Valor de Demanda",
      data: [200, null],
      backgroundColor: "#7C0AC1",
      pointStyle: "triangle",
    });
    expect(chartCall.data.datasets[1]).toEqual({
      label: "Valor de Consumo",
      data: [150, 180],
      backgroundColor: "#729BCA",
    });
  });

  it("configures chart options correctly", () => {
    render(<CurrentBaseCostPlot dates={mockDates} currentContractCostsPlot={mockGraphData} />);
    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    expect(chartCall.options.responsive).toBe(true);
    expect(chartCall.options.scales.y.title.text).toBe("R$");
    expect(chartCall.options.scales.x.grid.display).toBe(false);
  });

  it("handles tooltip callbacks correctly", () => {
    render(<CurrentBaseCostPlot dates={mockDates} currentContractCostsPlot={mockGraphData} />);
    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const tooltipCallbacks = chartCall.options.plugins.tooltip.callbacks;

    // Test title callback
    expect(tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: 200 } }])).toBe("Jan 2023");
    expect(tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: null } }])).toBe("Jan 2023 - Indisponível");

    // Test label callback
    expect(tooltipCallbacks.label({ parsed: { y: 200 }, dataset: { label: "Valor de Demanda" } })).toBe("Valor de Demanda: R$ 200,00");
    expect(tooltipCallbacks.label({ parsed: { y: null }, dataset: { label: "Valor de Demanda" } })).toBeNull();

    // Test footer callback
    expect(tooltipCallbacks.footer([{ parsed: { y: 200 } }, { parsed: { y: 150 } }])).toBe("Total: R$ 350,00");
    expect(tooltipCallbacks.footer([{ parsed: { y: null } }])).toBeNull();
  });

  it("handles null values in datalabels formatter", () => {
    render(<CurrentBaseCostPlot dates={mockDates} currentContractCostsPlot={mockGraphData} />);
    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const datalabelsFormatter = chartCall.options.plugins.datalabels.formatter;
    expect(datalabelsFormatter(null)).toBe("Indisponível");
    expect(datalabelsFormatter(200)).toBeNull();
  });
});