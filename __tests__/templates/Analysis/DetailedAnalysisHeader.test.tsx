import { render } from '@testing-library/react';
import DetailedAnalysisHeader from '../../../src/templates/Analysis/DetailedAnalysisHeader';
import '@testing-library/jest-dom';

describe('DetailedAnalysisHeader', () => {
  it('renders without children', () => {
    const { container } = render(<DetailedAnalysisHeader />);
    expect(container.firstChild).toBeInTheDocument();
  });

  it('renders with children', () => {
    const { getByText } = render(
      <DetailedAnalysisHeader>
        <div>Test Content</div>
      </DetailedAnalysisHeader>
    );
    expect(getByText('Test Content')).toBeInTheDocument();
  });

  it('renders with correct styles', () => {
    const { container } = render(<DetailedAnalysisHeader />);
    const header = container.firstChild;
    expect(header).toHaveStyle({
      position: 'sticky',
      top: '0',
      height: '64px'
    });
  });

  it('renders with grid container', () => {
    const { container } = render(<DetailedAnalysisHeader />);
    expect(container.querySelector('.MuiGrid-container')).toBeInTheDocument();
  });
});
