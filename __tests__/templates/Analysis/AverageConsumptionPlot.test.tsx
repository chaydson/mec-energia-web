import { render } from "@testing-library/react";
import { AverageConsumptionPlot } from "../../../src/templates/Analysis/AverageConsumptionPlot";
import { Chart } from "react-chartjs-2";

// Mock Chart.js
jest.mock("react-chartjs-2", () => ({
  Chart: jest.fn(() => null),
}));

describe("AverageConsumptionPlot", () => {
  const mockDates = [["Jan 2023"], ["Fev 2023"]];
  const mockData = {
    peakConsumptionInKwh: [100, 150],
    offPeakConsumptionInKwh: [200, 250],
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders with green dataset when isGreen is true", () => {
    render(
      <AverageConsumptionPlot
        dates={mockDates}
        data={mockData}
        isGreen={true}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    // Check if only one dataset is present for green mode
    expect(chartCall.data.datasets).toHaveLength(1);
    expect(chartCall.data.datasets[0]).toEqual({
      label: "Consumo",
      data: mockData.peakConsumptionInKwh,
      backgroundColor: "#003A7A",
      borderColor: "#003A7A",
      pointStyle: "triangle",
    });
  });

  it("renders with blue datasets when isGreen is false", () => {
    render(
      <AverageConsumptionPlot
        dates={mockDates}
        data={mockData}
        isGreen={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    // Check if two datasets are present for blue mode
    expect(chartCall.data.datasets).toHaveLength(2);
    expect(chartCall.data.datasets[0]).toEqual({
      label: "Consumo Ponta",
      data: mockData.peakConsumptionInKwh,
      backgroundColor: "#003A7A",
      borderColor: "#003A7A",
      pointStyle: "triangle",
    });
    expect(chartCall.data.datasets[1]).toEqual({
      label: "Consumo Fora Ponta",
      data: mockData.offPeakConsumptionInKwh,
      backgroundColor: "#729BCA",
      borderColor: "#729BCA",
      pointStyle: "circle",
    });
  });

  it("handles tooltip callbacks correctly", () => {
    render(
      <AverageConsumptionPlot
        dates={mockDates}
        data={mockData}
        isGreen={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const tooltipCallbacks = chartCall.options.plugins.tooltip.callbacks;

    // Test title callback
    const titleResult = tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: 100 } }]);
    expect(titleResult).toBe("Jan 2023");

    const titleResultNull = tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: null } }]);
    expect(titleResultNull).toBe("Jan 2023 - Indisponível");

    // Test label callback
    const labelResult = tooltipCallbacks.label({
      parsed: { y: 1000 },
      dataset: { label: "Consumo Ponta" },
    });
    expect(labelResult).toBe("Consumo Ponta: 1.000 kWh");

    const labelResultNull = tooltipCallbacks.label({
      parsed: { y: null },
      dataset: { label: "Consumo Ponta" },
    });
    expect(labelResultNull).toBeNull();
  });

  it("renders with correct chart options", () => {
    render(
      <AverageConsumptionPlot
        dates={mockDates}
        data={mockData}
        isGreen={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const options = chartCall.options;

    // Test chart configuration
    expect(options.responsive).toBe(true);
    expect(options.interaction).toEqual({
      intersect: false,
      mode: "nearest",
      axis: "x",
    });

    // Test scales configuration
    expect(options.scales.y.title).toEqual({
      display: true,
      text: "kWh",
    });
    expect(options.scales.x.grid.display).toBe(false);
    expect(options.scales.y.ticks.beginAtZero).toBe(true);
  });

  it("handles null values in datalabels formatter", () => {
    render(
      <AverageConsumptionPlot
        dates={mockDates}
        data={mockData}
        isGreen={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const datalabelsFormatter = chartCall.options.plugins.datalabels.formatter;

    expect(datalabelsFormatter(null)).toBe("Indisponível");
    expect(datalabelsFormatter(100)).toBeNull();
  });

  it("renders with correct style properties", () => {
    render(
      <AverageConsumptionPlot
        dates={mockDates}
        data={mockData}
        isGreen={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    expect(chartCall.style).toEqual({
      maxWidth: "100%",
      maxHeight: "350px",
    });
  });
});