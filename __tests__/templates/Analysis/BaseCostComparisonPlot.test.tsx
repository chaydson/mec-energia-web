import { render } from "@testing-library/react";
import { BaseCostComparisonPlot } from "../../../src/templates/Analysis/BaseCostComparisonPlot";
import { Chart } from "react-chartjs-2";
import theme from "@/theme";
import { Recommendation } from "@/types/recommendation";

// Mock Chart.js
jest.mock("react-chartjs-2", () => ({
  Chart: jest.fn(() => null),
}));

describe("BaseCostComparisonPlot", () => {
  const mockDates = [["Jan 2023"], ["Fev 2023"]];
  const mockRecommendation: Recommendation = {
    costsComparisonPlot: {
      totalCostInReaisInCurrent: [1000, 1500],
      totalCostInReaisInRecommended: [800, 1200],
    },
  } as Recommendation;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders with correct datasets", () => {
    render(
      <BaseCostComparisonPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    // Check if both datasets are present
    expect(chartCall.data.datasets).toHaveLength(2);
    expect(chartCall.data.datasets[0]).toEqual({
      label: "Consumo + Demanda atuais",
      data: mockRecommendation.costsComparisonPlot.totalCostInReaisInCurrent,
      backgroundColor: theme.palette.graph.baseCostMain,
      pointStyle: "rect",
      stack: "Atual",
    });
    expect(chartCall.data.datasets[1]).toEqual({
      label: "Consumo + Demanda propostos",
      data: mockRecommendation.costsComparisonPlot.totalCostInReaisInRecommended,
      backgroundColor: theme.palette.graph.baseCostSecondary,
      pointStyle: "circle",
      stack: "Proposto",
    });
  });

  it("handles tooltip callbacks correctly", () => {
    render(
      <BaseCostComparisonPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const tooltipCallbacks = chartCall.options.plugins.tooltip.callbacks;

    // Test title callback
    const titleResult = tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: 1000 } }]);
    expect(titleResult).toBe("Jan 2023");

    const titleResultNull = tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: null } }]);
    expect(titleResultNull).toBe("Jan 2023 - Indisponível");

    const labelResultNull = tooltipCallbacks.label({
      parsed: { y: null },
      dataset: { label: "Consumo + Demanda atuais" },
    });
    expect(labelResultNull).toBeNull();
  });

  it("renders with correct chart options", () => {
    render(
      <BaseCostComparisonPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const options = chartCall.options;

    // Test chart configuration
    expect(options.responsive).toBe(true);
    expect(options.interaction).toEqual({
      intersect: false,
      mode: "nearest",
      axis: "x",
    });

    // Test scales configuration
    expect(options.scales.y.title).toEqual({
      display: true,
      text: "R$",
    });
    expect(options.scales.x.grid.display).toBe(false);
    expect(options.scales.y.grid.color).toBe("#C3C3C3");
  });

  it("handles null values in datalabels formatter", () => {
    render(
      <BaseCostComparisonPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const datalabelsFormatter = chartCall.options.plugins.datalabels.formatter;

    expect(datalabelsFormatter(null)).toBe("Indisponível");
    expect(datalabelsFormatter(1000)).toBeNull();
  });

  it("configures legend with correct options", () => {
    render(
      <BaseCostComparisonPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const legendOptions = chartCall.options.plugins.legend;

    expect(legendOptions).toEqual({
      position: "bottom",
      labels: {
        usePointStyle: true,
      },
    });
  });

  it("configures bar dataset options correctly", () => {
    render(
      <BaseCostComparisonPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const barOptions = chartCall.options.datasets.bar;

    expect(barOptions).toEqual({
      barPercentage: 1,
      skipNull: true,
    });
  });
});