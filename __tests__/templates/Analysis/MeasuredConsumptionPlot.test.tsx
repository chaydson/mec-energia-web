import { render } from "@testing-library/react";
import { MeasuredConsumptionPlot } from "../../../src/templates/Analysis/MeasuredConsumptionPlot";
import { Chart } from "react-chartjs-2";
import { Recommendation } from "@/types/recommendation";
import theme from "@/theme";

// Mock Chart.js
jest.mock("react-chartjs-2", () => ({
  Chart: jest.fn(() => null),
}));

describe("MeasuredConsumptionPlot", () => {
  const mockDates = [["Jan/2023"], ["Fev/2023"]];
  const mockRecommendation: Recommendation = {
    consumptionHistoryPlot: {
      peakConsumptionInKwh: [1000, null],
      offPeakConsumptionInKwh: [2000, 2500],
    },
  } as Recommendation;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders with correct datasets and handles null values", () => {
    render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    // Check if both datasets are present with correct data transformation
    expect(chartCall.data.datasets).toHaveLength(2);
    expect(chartCall.data.datasets[0]).toEqual({
      label: "Consumo Ponta",
      data: [1000, null],
      backgroundColor: theme.palette.graph.measuredConsumptionMain,
      borderColor: theme.palette.graph.measuredConsumptionMain,
      borderWidth: 1,
      pointStyle: "triangle",
    });
    expect(chartCall.data.datasets[1]).toEqual({
      label: "Consumo Fora ponta",
      data: [2000, 2500],
      backgroundColor: theme.palette.graph.measuredConsumptionSecondary,
      borderColor: theme.palette.graph.measuredConsumptionSecondary,
      borderWidth: 1,
    });
  });

  it("handles tooltip callbacks correctly", () => {
    render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const tooltipCallbacks = chartCall.options.plugins.tooltip.callbacks;

    // Test title callback with date formatting
    const titleResult = tooltipCallbacks.title([{ label: "Jan,2023" }]);
    expect(titleResult).toBe("Jan/2023");

    // Test label callback with normal value
    const labelResult = tooltipCallbacks.label({
      parsed: { y: 1000 },
      dataset: { label: "Ponta" },
    });
    expect(labelResult).toBe("Consumo Ponta: 1.000 kWh");

    // Test label callback with null value
    const labelNullResult = tooltipCallbacks.label({
      parsed: { y: null },
      dataset: { label: "Ponta" },
    });
    expect(labelNullResult).toBe("Consumo Ponta: ");
  });

  it("renders with correct chart options", () => {
    render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const options = chartCall.options;

    // Test chart configuration
    expect(options.responsive).toBe(true);
    expect(options.interaction).toEqual({
      intersect: false,
      mode: "nearest",
      axis: "x",
    });

    // Test scales configuration
    expect(options.scales.y.title).toEqual({
      display: true,
      text: "kWh",
    });
    expect(options.scales.x.grid.display).toBe(false);
    expect(options.scales.y.grid.color).toBe("#C3C3C3");
    expect(options.scales.x.ticks.maxRotation).toBe(0);
  });

  it("configures legend with correct options", () => {
    render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const legendOptions = chartCall.options.plugins.legend;

    expect(legendOptions).toEqual({
      position: "bottom",
      labels: {
        usePointStyle: true,
      },
    });
  });

  it("configures bar dataset options correctly", () => {
    render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const barOptions = chartCall.options.datasets.bar;

    expect(barOptions).toEqual({
      barPercentage: 1,
    });
  });

  it("sets correct datasetIdKey", () => {
    render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    expect(chartCall.datasetIdKey).toBe("measured-consumption");
  });

  it("applies correct Box margin style", () => {
    const { container } = render(
      <MeasuredConsumptionPlot
        dates={mockDates}
        recommendation={mockRecommendation}
      />
    );

    const boxElement = container.firstChild;
    expect(boxElement).toHaveStyle({ marginTop: "16px" }); // mt={2} equals 16px
  });
});