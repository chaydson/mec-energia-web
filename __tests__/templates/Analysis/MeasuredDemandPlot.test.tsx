import { render, screen } from "@testing-library/react";
import { MeasuredDemandPlot } from "../../../src/templates/Analysis/MeasuredDemandPlot";
import { Chart } from "react-chartjs-2";
import theme from "@/theme";
import { ConsumerUnitGraphResponse } from "@/types/graphs";

// Mock Chart.js
jest.mock("react-chartjs-2", () => ({
  Chart: jest.fn(() => null),
}));

describe("MeasuredDemandPlot", () => {
  const mockDates = [["Jan/2023"], ["Fev/2023"]];
  const mockGraphData: ConsumerUnitGraphResponse = {
    contractData: {
      peakContractedDemandInKw: 100,
      offPeakContractedDemandInKw: 150,
    },
    consumptionHistoryPlot: {
      peakMeasuredDemandInKw: [90, null],
      offPeakMeasuredDemandInKw: [140, 145],
    },
  } as ConsumerUnitGraphResponse;

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders with green datasets when isGreen is true", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={true}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    // Check datasets in green mode
    expect(chartCall.data.datasets).toHaveLength(3);
    expect(chartCall.data.datasets[0]).toEqual({
      type: "line",
      label: "Demanda Contratada",
      data: Array(12).fill(100),
      backgroundColor: "#008940",
      borderColor: "#008940",
      pointStyle: "rect",
      pointRadius: 4,
    });
    expect(chartCall.data.datasets[1]).toEqual({
      type: "bar",
      label: "Demanda Medida Ponta",
      data: [90, null],
      backgroundColor: "#7C0AC1",
      borderColor: "#7C0AC1",
    });
    expect(chartCall.data.datasets[2]).toEqual({
      type: "bar",
      label: "Demanda Medida Fora Ponta",
      data: [140, 145],
      backgroundColor: "#CB95EC",
      borderColor: "#CB95EC",
      pointStyle: "circle",
    });
  });

  it("renders with blue datasets when isGreen is false", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];

    // Check datasets in blue mode
    expect(chartCall.data.datasets).toHaveLength(4);
    expect(chartCall.data.datasets[0]).toEqual({
      type: "line",
      label: "Demanda Contratada Ponta",
      data: Array(12).fill(100),
      backgroundColor: theme.palette.graph.measuredDemandPeakLine,
      borderColor: theme.palette.graph.measuredDemandPeakLine,
      pointStyle: "rectRot",
      pointRadius: 4,
    });
    expect(chartCall.data.datasets[3]).toEqual({
      type: "bar",
      label: "Demanda Medida Fora Ponta",
      data: [140, 145],
      backgroundColor: theme.palette.graph.measuredDemandSecondary,
      borderColor: theme.palette.graph.measuredDemandSecondary,
      pointStyle: "circle",
    });
  });

  it("handles tooltip callbacks correctly", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const tooltipCallbacks = chartCall.options.plugins.tooltip.callbacks;

    // Test title callback
    const titleResult = tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: 100 } }]);
    expect(titleResult).toBe("Jan 2023");

    const titleResultNull = tooltipCallbacks.title([{ label: "Jan,2023", parsed: { y: null } }]);
    expect(titleResultNull).toBe("Jan 2023 - Indisponível");

    // Test label callback
    const labelResult = tooltipCallbacks.label({
      parsed: { y: 100 },
      dataset: { label: "Demanda Contratada Ponta" },
    });
    expect(labelResult).toBe("Demanda Contratada Ponta: 100 kW");

    const labelNullResult = tooltipCallbacks.label({
      parsed: { y: null },
      dataset: { label: "Demanda Contratada Ponta" },
    });
    expect(labelNullResult).toBe("Demanda Contratada Ponta: Indisponível");
  });

  it("renders subtitle when isDetailedAnalysis is true", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={true}
      />
    );

    expect(screen.getByText(/Figura 3/)).toBeInTheDocument();
    expect(screen.getByText(/Gráfico comparativo/)).toBeInTheDocument();
  });

  it("does not render subtitle when isDetailedAnalysis is false", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    expect(screen.queryByText(/Figura 3/)).not.toBeInTheDocument();
  });

  it("configures chart options correctly", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const options = chartCall.options;

    expect(options.responsive).toBe(true);
    expect(options.interaction).toEqual({
      intersect: false,
      mode: "index",
      axis: "x",
    });

    expect(options.scales.y.title).toEqual({
      display: true,
      text: "kW",
    });
    expect(options.scales.y.ticks.beginAtZero).toBe(true);
    expect(options.scales.x.grid.display).toBe(false);
  });

  it("handles null values in datalabels formatter", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    const datalabelsFormatter = chartCall.options.plugins.datalabels.formatter;

    expect(datalabelsFormatter(null)).toBe("Indisponível");
    expect(datalabelsFormatter(100)).toBeNull();
  });

  it("applies correct Box styles", () => {
    const { container } = render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    const boxElement = container.firstChild;
    expect(boxElement).toHaveStyle({
      width: "100%",
      height: "250px",
      marginTop: "32px", // mt={4}
    });
  });

  it("sets correct chart type and datasetIdKey", () => {
    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={mockGraphData}
        isGreen={false}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    expect(chartCall.type).toBe("line");
    expect(chartCall.datasetIdKey).toBe("measured-demand");
  });

  it("handles case when no valid peak demand data exists", () => {
    const noValidPeakData = {
      ...mockGraphData,
      consumptionHistoryPlot: {
        ...mockGraphData.consumptionHistoryPlot,
        peakMeasuredDemandInKw: [null, null],
      },
    };

    render(
      <MeasuredDemandPlot
        dates={mockDates}
        graphData={noValidPeakData}
        isGreen={true}
        isDetailedAnalysis={false}
      />
    );

    const chartCall = (Chart as jest.Mock).mock.calls[0][0];
    expect(chartCall.data.datasets).toHaveLength(2);
  });
});