import { render } from '@testing-library/react';
import { BaseCostComparisonCard } from '../../../src/templates/Analysis/BaseCostComparisonCard';
import '@testing-library/jest-dom';

// Mock the child components
jest.mock('../../../src/templates/Analysis/BaseCostComparisonPlot', () => ({
  BaseCostComparisonPlot: () => null
}));
jest.mock('../../../src/templates/Analysis/CurrentBaseCostPlot', () => ({
  CurrentBaseCostPlot: () => null
}));
jest.mock('@/components/ConsumerUnit/Content/BaseCostInfoModal', () => ({
  BaseCostInfoModal: () => null
}));

describe('BaseCostComparisonCard', () => {
  const mockDates = [['Jan'], ['Fev']];
  const mockRecommendation = {
    id: 1,
    consumerUnitId: 1,
    date: '2025-01-01',
    currentContract: {
      id: 1,
      startDate: '2025-01-01',
      endDate: '2025-12-31',
      peakContractedDemandInKw: 100,
      offPeakContractedDemandInKw: 100,
      supplyVoltage: 'A4',
      tariffFlag: 'VERDE',
      subgroup: 'A4',
    },
    recommendedContract: {
      id: 2,
      startDate: '2025-01-01',
      endDate: '2025-12-31',
      peakContractedDemandInKw: 150,
      offPeakContractedDemandInKw: 150,
      supplyVoltage: 'A4',
      tariffFlag: 'VERDE',
      subgroup: 'A4',
    },
    costsComparisonPlot: {
      totalCostInReaisInCurrent: [1000, 1200],
      totalCostInReaisInRecommended: [800, 900]
    },
    nominalSavingsPercentage: 25.0,
  };

  it('renders error message when hasErrors is true', () => {
    const { container } = render(
      <BaseCostComparisonCard
        dates={mockDates}
        hasErrors={true}
        recommendation={mockRecommendation}
        hasRecommendation={true}
      />
    );
    const cardContent = container.querySelector('.MuiCardContent-root');
    expect(cardContent).toHaveTextContent('Indisponível');
  });

  it('renders without errors when hasErrors is false', () => {
    const { container } = render(
      <BaseCostComparisonCard
        dates={mockDates}
        hasErrors={false}
        recommendation={mockRecommendation}
        hasRecommendation={true}
      />
    );
    expect(container.firstChild).toBeInTheDocument();
  });

  it('renders without recommendation when hasRecommendation is false', () => {
    const { container } = render(
      <BaseCostComparisonCard
        dates={mockDates}
        hasErrors={false}
        recommendation={mockRecommendation}
        hasRecommendation={false}
      />
    );
    expect(container.firstChild).toBeInTheDocument();
  });
});
