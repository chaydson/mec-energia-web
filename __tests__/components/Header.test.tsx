import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Header from '../../src/components/Header';
import { useRouter } from 'next/router';
import { getRouteByPathname } from '../../src/routes';

// Mock do next/router
jest.mock('next/router', () => ({
  useRouter: jest.fn()
}));

// Mock da função getRouteByPathname
jest.mock('../../src/routes', () => ({
  getRouteByPathname: jest.fn()
}));

describe('Header', () => {
  beforeEach(() => {
    // Reset dos mocks antes de cada teste
    jest.clearAllMocks();
  });

  it('deve renderizar corretamente com uma rota válida', () => {
    // Arrange
    const mockRoute = {
      title: 'Página Teste',
      Icon: () => <div data-testid="mock-icon">Icon</div>
    };
    
    (useRouter as jest.Mock).mockReturnValue({ pathname: '/test' });
    (getRouteByPathname as jest.Mock).mockReturnValue(mockRoute);

    // Act
    render(<Header />);

    // Assert
    expect(screen.getByText('Página Teste')).toBeInTheDocument();
    expect(screen.getByTestId('mock-icon')).toBeInTheDocument();
  });

  it('deve renderizar children quando fornecidos', () => {
    // Arrange
    const mockRoute = {
      title: 'Página Teste',
      Icon: () => <div data-testid="mock-icon">Icon</div>
    };
    
    (useRouter as jest.Mock).mockReturnValue({ pathname: '/test' });
    (getRouteByPathname as jest.Mock).mockReturnValue(mockRoute);

    // Act
    render(
      <Header>
        <div data-testid="child-content">Conteúdo Filho</div>
      </Header>
    );

    // Assert
    expect(screen.getByTestId('child-content')).toBeInTheDocument();
  });

  it('não deve renderizar título e ícone quando não há rota correspondente', () => {
    // Arrange
    (useRouter as jest.Mock).mockReturnValue({ pathname: '/invalid' });
    (getRouteByPathname as jest.Mock).mockReturnValue(null);

    // Act
    render(<Header />);

    // Assert
    expect(screen.queryByRole('heading')).not.toBeInTheDocument();
  });
});
