import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import FormFieldError from '../../src/components/FormFieldError';

describe('FormFieldError', () => {
  it('deve renderizar a mensagem de erro com ícone quando errorMessage é fornecida', () => {
    // Arrange
    const errorMessage = 'Erro de teste';

    // Act
    render(FormFieldError(errorMessage));

    // Assert
    expect(screen.getByText('Erro de teste')).toBeInTheDocument();
    // Verifica se o ícone está presente
    expect(screen.getByTestId('ReportRoundedIcon')).toBeInTheDocument();
  });

  it('deve renderizar helperMessage quando não há errorMessage', () => {
    // Arrange
    const helperMessage = 'Mensagem de ajuda';

    // Act
    render(FormFieldError(undefined, helperMessage));

    // Assert
    expect(screen.getByText('Mensagem de ajuda')).toBeInTheDocument();
    // Verifica que o ícone não está presente
    expect(screen.queryByTestId('ReportRoundedIcon')).not.toBeInTheDocument();
  });

  it('deve renderizar espaço em branco quando nem errorMessage nem helperMessage são fornecidos', () => {
    // Act
    const { container } = render(FormFieldError());

    // Assert
    // Verifica se o texto renderizado é exatamente um espaço em branco
    expect(container.textContent).toBe(' ');
    expect(screen.queryByTestId('ReportRoundedIcon')).not.toBeInTheDocument();
  });
});
