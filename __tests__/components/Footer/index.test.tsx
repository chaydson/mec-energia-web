import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import Footer from "@/components/Footer";

describe("Footer", () => {
  it("deve renderizar corretamente", () => {
    render(<Footer />);

    // Verifica se o texto principal do rodapé está presente
    expect(screen.getByText("Monitoramento de Energia em Plataforma Aberta 2025")).toBeInTheDocument();
  });

  it("deve renderizar a logo do MEPA", () => {
    render(<Footer />);

    const logoMepa = screen.getByAltText("MEPA");
    expect(logoMepa).toBeInTheDocument();
  });

  it("deve renderizar o texto do rodapé corretamente", () => {
    render(<Footer />);

    const textoRodape = screen.getByText("Monitoramento de Energia em Plataforma Aberta 2025");
    expect(textoRodape).toBeInTheDocument();
  });

  it("deve renderizar as logos da UnB e do Governo Federal", () => {
    render(<Footer />);

    const logoUnB = screen.getByAltText("UnB");
    const logoGoverno = screen.getByAltText("Governo Federal");

    expect(logoUnB).toBeInTheDocument();
    expect(logoGoverno).toBeInTheDocument();
  });
});
