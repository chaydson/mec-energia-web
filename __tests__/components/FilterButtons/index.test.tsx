import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import FilterButtons from "@/components/FilterButtons";
import { SvgIcon } from "@mui/material";

// Mock do componente SvgIcon para simplificar
jest.mock("@mui/material", () => ({
  ...jest.requireActual("@mui/material"),
  SvgIcon: (props: React.ComponentProps<typeof SvgIcon>) => (
    <div data-testid="mock-icon" {...props} />
  ),
}));

describe("FilterButtons", () => {
  const mockOnClick = jest.fn();

  const buttons = [
    { active: true, Icon: SvgIcon, title: "Ativo", onClick: mockOnClick },
    { active: false, Icon: SvgIcon, title: "Inativo", onClick: mockOnClick },
  ];

  it("deve renderizar corretamente os botões", () => {
    render(<FilterButtons buttons={buttons} />);

    // Verifica se os botões foram renderizados
    expect(screen.getByRole("button", { name: "Ativo" })).toBeInTheDocument();

    // Para o botão inativo, use getAllByRole e verifique o segundo botão
    const renderedButtons = screen.getAllByRole("button");
    expect(renderedButtons[1]).toBeInTheDocument(); // Segundo botão é o inativo
  });

  it("deve chamar a função onClick quando um botão é clicado", () => {
    render(<FilterButtons buttons={buttons} />);

    // Clica no botão "Ativo"
    fireEvent.click(screen.getByRole("button", { name: "Ativo" }));

    // Verifica se a função onClick foi chamada
    expect(mockOnClick).toHaveBeenCalledTimes(1);
  });

  it("não deve exibir o título quando o botão não está ativo", () => {
    render(<FilterButtons buttons={buttons} />);

    // Verifica que o título "Inativo" não é exibido para o botão inativo
    expect(screen.queryByText("Inativo")).toBeNull();
  });
});