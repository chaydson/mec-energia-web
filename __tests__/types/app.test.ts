import { STORE_HYDRATE, ConsumerUnitTab, TokenStatus, RootState, CardProps, NotificationProps, EnergyBillEdiFormParams } from '../../src/types/app';

describe("app.ts", () => {
  it("deve criar a ação STORE_HYDRATE corretamente", () => {
    const mockState: RootState = {
      consumerUnit: {
        activeId: null,
        isCreateFormOpen: false,
        isEditFormOpen: false,
        isCsvFormOpen: false,
        isRenewContractFormOpen: false,
        activeFilter: "all",
        openedTab: ConsumerUnitTab.INVOICE,
        invoice: {
          activeFilter: "pending",
          dataGridRows: [],
        },
      },
      dashboard: { activeFilter: "all" },
      distributor: {
        activeId: null,
        activeSubgroup: null,
        isCreateFormOpen: false,
        isEditFormOpen: false,
      },
      energyBill: {
        isCreateFormOpen: false,
        isEditFormOpen: false,
        params: { month: null, year: null, id: null },
      },
      institution: {
        activeId: null,
        isCreateFormOpen: false,
        isEditFormOpen: false,
      },
      person: {
        activeId: null,
        isCreateFormOpen: false,
        isEditFormOpen: false,
        isEditPasswordFormOpen: false,
      },
      notifications: {
        success: { isOpen: false, text: "" },
        error: { isOpen: false, text: "" },
      },
      tariff: {
        isCreateFormOpen: false,
        isEditFormOpen: false,
      },
      token: {
        status: null,
        passwordAlreadyCreated: null,
        userName: undefined,
      },
    };

    const action = STORE_HYDRATE(mockState);
    expect(action.type).toBe("__NEXT_REDUX_WRAPPER_HYDRATE__");
    expect(action.payload).toEqual(mockState);
  });

  it("deve ter os valores corretos para ConsumerUnitTab", () => {
    expect(ConsumerUnitTab.INVOICE).toBe(0);
    expect(ConsumerUnitTab.ANALYSIS).toBe(1);
    expect(ConsumerUnitTab.CONTRACT).toBe(2);
  });

  it("deve ter os valores corretos para TokenStatus", () => {
    expect(TokenStatus.RESET_PASSWORD_INVALID).toBe("reset_password_invalid");
    expect(TokenStatus.RESET_PASSWORD).toBe("reset_password");
    expect(TokenStatus.FIRST_TIME_CREATION_INVALID).toBe("first_time_creation_invalid");
    expect(TokenStatus.FIRST_TIME_CREATION).toBe("first_time_creation");
    expect(TokenStatus.TOKEN_ALREADY_USED).toBe("token_already_used");
  });

  it("deve aceitar valores válidos para as interfaces exportadas", () => {
    const notification: NotificationProps = {
      isOpen: true,
      text: "Mensagem de sucesso",
    };

    const energyBillParams: EnergyBillEdiFormParams = {
      month: 1,
      year: 2023,
      id: 123,
    };

    const cardProps: CardProps = {
      name: "Test Card",
      isFavorite: true,
    };

    expect(notification.isOpen).toBe(true);
    expect(notification.text).toBe("Mensagem de sucesso");

    expect(energyBillParams.month).toBe(1);
    expect(energyBillParams.year).toBe(2023);
    expect(energyBillParams.id).toBe(123);

    expect(cardProps.name).toBe("Test Card");
    expect(cardProps.isFavorite).toBe(true);
  });
});
