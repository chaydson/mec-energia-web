import {
  GetContractsResponsePayload,
  RenewContractForm,
  RenewContractRequestPayload,
  RenewContractResponsePayload
} from '../../src/types/contract';
import '@testing-library/jest-dom';

const validateGetContractsResponsePayload = (data: GetContractsResponsePayload): boolean => {
  return (
    typeof data.url === 'string' &&
    typeof data.id === 'number' &&
    typeof data.consumerUnit === 'number' &&
    typeof data.distributorName === 'string' &&
    typeof data.distributor === 'number' &&
    typeof data.startDate === 'string' &&
    data.endDate instanceof Date &&
    typeof data.tariffFlag === 'string' &&
    typeof data.subgroup === 'string' &&
    typeof data.peakContractedDemandInKw === 'number' &&
    typeof data.offPeakContractedDemandInKw === 'number'
  );
};

describe('Contract Payloads', () => {
  test('GetContractsResponsePayload deve aceitar uma estrutura válida', () => {
    const validPayload: GetContractsResponsePayload = {
      url: 'https://example.com/contract/123',
      id: 123,
      consumerUnit: 456,
      distributorName: 'Distribuidora XYZ',
      distributor: 789,
      startDate: '2023-01-01',
      endDate: new Date('2024-01-01'),
      tariffFlag: 'GREEN',
      subgroup: 'A4',
      peakContractedDemandInKw: 100,
      offPeakContractedDemandInKw: 50,
    };

    expect(validPayload).toBeDefined();
    expect(validPayload.id).toBe(123);
    expect(validPayload.distributorName).toBe('Distribuidora XYZ');
  });

  test('RenewContractForm deve aceitar uma estrutura válida', () => {
    const validForm: RenewContractForm = {
      code: 'CONTRACT123',
      distributor: 789,
      startDate: new Date('2023-01-01'),
      subgroup: 'A4',
      tariffFlag: 'GREEN',
      contracted: 100,
      peakContractedDemandInKw: 120,
      offPeakContractedDemandInKw: 60,
    };

    expect(validForm).toBeDefined();
    expect(validForm.code).toBe('CONTRACT123');
    expect(validForm.startDate).toEqual(new Date('2023-01-01'));
  });

  test('RenewContractRequestPayload deve aceitar uma estrutura válida', () => {
    const validRequest: RenewContractRequestPayload = {
      consumerUnit: 456,
      code: 'CONTRACT123',
      distributor: 789,
      startDate: '2023-01-01',
      subgroup: 'A4',
      tariffFlag: 'GREEN',
      peakContractedDemandInKw: 120,
      offPeakContractedDemandInKw: 60,
    };

    expect(validRequest).toBeDefined();
    expect(validRequest.consumerUnit).toBe(456);
    expect(validRequest.code).toBe('CONTRACT123');
  });

  test('RenewContractResponsePayload deve aceitar uma estrutura válida', () => {
    const validResponse: RenewContractResponsePayload = {
      id: 123,
      consumerUnit: 456,
      distributor: 789,
      startDate: '2023-01-01',
      endDate: null,
      tariffFlag: 'GREEN',
      subgroup: 'A4',
      peakContractedDemandInKw: 120,
      offPeakContractedDemandInKw: 60,
    };

    expect(validResponse).toBeDefined();
    expect(validResponse.id).toBe(123);
    expect(validResponse.endDate).toBeNull();
  });

  test('deve falhar se tipos de dados estiverem incorretos em GetContractsResponsePayload', () => {
    const invalidPayload = {
      url: 123,
      id: '123',
      consumerUnit: 456,
      distributorName: 'Distribuidora XYZ',
      distributor: 789,
      startDate: '2023-01-01',
      endDate: new Date('2024-01-01'),
      tariffFlag: 'GREEN',
      subgroup: 'A4',
      peakContractedDemandInKw: 100,
      offPeakContractedDemandInKw: 50,
    };

    const isValid = validateGetContractsResponsePayload(invalidPayload);
    expect(isValid).toBe(false);
  });

  test('deve suportar valores opcionais ou nulos em RenewContractForm', () => {
    const validForm: RenewContractForm = {
      code: 'CONTRACT123',
      distributor: '',
      startDate: null,
      subgroup: '',
      tariffFlag: 'RED',
      contracted: '',
      peakContractedDemandInKw: '',
      offPeakContractedDemandInKw: '',
    };

    expect(validForm).toBeDefined();
    expect(validForm.startDate).toBeNull();
    expect(validForm.distributor).toBe('');
  });
});
