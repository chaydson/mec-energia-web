import { ConsumerUnitGraphResponse } from '../../src/types/graphs';
import '@testing-library/jest-dom';

describe('ConsumerUnitGraphResponse Interface', () => {
  const validResponse: ConsumerUnitGraphResponse = {
    contractData: {
      peakContractedDemandInKw: 50,
      offPeakContractedDemandInKw: 30,
    },
    consumptionHistoryPlot: {
      date: ['2023-01-01', '2023-01-02', '2023-01-03'],
      peakConsumptionInKwh: [100, 200, 150],
      offPeakConsumptionInKwh: [50, 70, 60],
      peakMeasuredDemandInKw: [55, null, 52],
      offPeakMeasuredDemandInKw: [30, 25, 28],
    },
  };

  test('deve ser válido com todos os campos preenchidos corretamente', () => {
    expect(validResponse).toBeDefined();
    expect(validResponse.contractData.peakContractedDemandInKw).toBe(50);
  });

  test('deve falhar ao faltar campos obrigatórios', () => {
    const invalidResponse = {
      contractData: {
        peakContractedDemandInKw: 50,
      },
      consumptionHistoryPlot: {
        date: ['2023-01-01'],
        peakConsumptionInKwh: [100],
        offPeakConsumptionInKwh: [50],
        peakMeasuredDemandInKw: [null],
        offPeakMeasuredDemandInKw: [30],
      },
    };

    const response: ConsumerUnitGraphResponse = invalidResponse;

    expect(response).toBeDefined();
  });

  test('deve falhar se os tipos de dados estiverem incorretos', () => {
    const invalidResponse = {
      contractData: {
        peakContractedDemandInKw: '50', // Tipo incorreto
        offPeakContractedDemandInKw: 30,
      },
      consumptionHistoryPlot: {
        date: ['2023-01-01'],
        peakConsumptionInKwh: [100],
        offPeakConsumptionInKwh: [50],
        peakMeasuredDemandInKw: [null],
        offPeakMeasuredDemandInKw: [30],
      },
    };

    const response: ConsumerUnitGraphResponse = invalidResponse;

    expect(response).toBeDefined();
  });

  test('deve suportar valores null em "peakMeasuredDemandInKw"', () => {
    const responseWithNull: ConsumerUnitGraphResponse = {
      contractData: {
        peakContractedDemandInKw: 45,
        offPeakContractedDemandInKw: 25,
      },
      consumptionHistoryPlot: {
        date: ['2023-01-01'],
        peakConsumptionInKwh: [100],
        offPeakConsumptionInKwh: [50],
        peakMeasuredDemandInKw: [null],
        offPeakMeasuredDemandInKw: [30],
      },
    };

    expect(responseWithNull.consumptionHistoryPlot.peakMeasuredDemandInKw).toEqual([null]);
  });
});
