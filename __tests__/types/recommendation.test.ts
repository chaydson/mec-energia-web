import { Recommendation, RecommendationSettings, RecommendationContract, tariffLabelToPtBr, TariffsTableRow } from '../../src/types/recommendation';


describe('Recommendation Interfaces and Constants', () => {
  it('should create a valid Recommendation object', () => {
    const recommendation: Recommendation = {
      generatedOn: '2025-01-14',
      errors: [[1, 'Error example']],
      warnings: [[2, 'Warning example']],
      dates: ['2024-12-01', '2024-12-31'],
      currentContract: {
        university: 'Test University',
        distributor: 'Test Distributor',
        consumerUnit: '1234',
        consumerUnitCode: '5678',
        tariffFlag: "G",
        subgroup: 'Test Subgroup',
        peakDemandInKw: 100,
        offPeakDemandInKw: 80,
      },
      energyBillsCount: 12,
      shouldRenewContract: true,
      nominalSavingsPercentage: 15,
      consumptionHistoryPlot: {
        peakConsumptionInKwh: [1000, 1200],
        offPeakConsumptionInKwh: [800, 900],
        peakMeasuredDemandInKw: [110, 120],
        offPeakMeasuredDemandInKw: [85, 90],
      },
      consumptionHistoryTable: [
        {
          date: '2024-12-01',
          peakConsumptionInKwh: 1000,
          offPeakConsumptionInKwh: 800,
          peakMeasuredDemandInKw: 110,
          offPeakMeasuredDemandInKw: 85,
        },
      ],
      currentContractCostsPlot: {
        consumptionCostInReais: [10000, 12000],
        demandCostInReais: [5000, 6000],
      },
      detailedContractsCostsComparisonPlot: {
        consumptionCostInReaisInRecommended: [8000, 10000],
        demandCostInReaisInRecommended: [4000, 5000],
        totalCostInReaisInCurrent: [15000, 18000],
      },
      costsComparisonPlot: {
        totalCostInReaisInCurrent: [15000, 18000],
        totalCostInReaisInRecommended: [12000, 15000],
        totalTotalCostInReaisInCurrent: 33000,
        totalTotalCostInReaisInRecommended: 27000,
      },
      recommendedContract: {
        university: 'Test University',
        distributor: 'Recommended Distributor',
        consumerUnit: '1234',
        consumerUnitCode: '5678',
        tariffFlag: "B",
        subgroup: 'Test Subgroup',
        peakDemandInKw: 80,
        offPeakDemandInKw: 60,
      },
      tariffDates: {
        startDate: '2024-01-01',
        endDate: '2024-12-31',
      },
      tariffsTable: [
        {
          label: 'peak_te_in_reais_per_mwh',
          blue: 450,
          green: 400,
          billingTime: 'Ponta',
        },
      ],
      contractsComparisonTable: [
        {
          date: '2024-12-01',
          totalCostInReaisInCurrent: 15000,
          demandCostInReaisInCurrent: 5000,
          consumptionCostInReaisInCurrent: 10000,
          totalCostInReaisInRecommended: 12000,
          demandCostInReaisInRecommended: 4000,
          consumptionCostInReaisInRecommended: 8000,
          absoluteDifference: 3000,
        },
      ],
      contractsComparisonTotals: {
        absoluteDifference: 6000,
        consumptionCostInReaisInRecommended: 18000,
        demandCostInReaisInRecommended: 9000,
        totalCostInReaisInRecommended: 27000,
        consumptionCostInReaisInCurrent: 20000,
        demandCostInReaisInCurrent: 13000,
        totalCostInReaisInCurrent: 33000,
      },
      currentTotalCost: 33000,
    };

    expect(recommendation).toBeDefined();
    expect(recommendation.generatedOn).toBe('2025-01-14');
    expect(recommendation.energyBillsCount).toBe(12);
    expect(recommendation.shouldRenewContract).toBe(true);
  });

  it('should validate RecommendationContract', () => {
    const contract: RecommendationContract = {
      university: 'Test University',
      distributor: 'Test Distributor',
      consumerUnit: '1234',
      consumerUnitCode: '5678',
      tariffFlag: "G",
      subgroup: 'Test Subgroup',
      peakDemandInKw: 100,
      offPeakDemandInKw: 80,
    };

    expect(contract.university).toBe('Test University');
    expect(contract.tariffFlag).toBe("G");
  });

  it('should validate tariffLabelToPtBr constants', () => {
    expect(tariffLabelToPtBr.peak_te_in_reais_per_mwh).toBe('Tarifa de energia (TE) ponta (R$/MWh)');
    expect(tariffLabelToPtBr.off_peak_te_in_reais_per_mwh).toBe('Tarifa de energia (TE) fora ponta (R$/MWh)');
  });

  it('should validate TariffsTableRow', () => {
    const tariffRow: TariffsTableRow = {
      label: 'peak_te_in_reais_per_mwh',
      blue: 450,
      green: 400,
      billingTime: 'Ponta',
    };

    expect(tariffRow.label).toBe('peak_te_in_reais_per_mwh');
    expect(tariffRow.blue).toBe(450);
    expect(tariffRow.billingTime).toBe('Ponta');
  });

  it('should validate RecommendationSettings', () => {
    const settings: RecommendationSettings = {
      MINIMUM_PERCENTAGE_DIFFERENCE_FOR_CONTRACT_RENOVATION: 10,
      MINIMUM_ENERGY_BILLS_FOR_RECOMMENDATION: 3,
      IDEAL_ENERGY_BILLS_FOR_RECOMMENDATION: 12,
    };

    expect(settings.MINIMUM_PERCENTAGE_DIFFERENCE_FOR_CONTRACT_RENOVATION).toBe(10);
    expect(settings.MINIMUM_ENERGY_BILLS_FOR_RECOMMENDATION).toBe(3);
  });
});
