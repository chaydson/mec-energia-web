import {
  CreatePersonForm,
  EditFavoritesRequestPayload,
  EditPasswordRequestPayload,
  PatchUserRequestPayload,
  UserRole,
  User,

} from '../../src/types/person';

describe('UserRole Enum', () => {
  test('should have correct roles defined', () => {
    expect(UserRole.SUPER_USER).toBe('super_user');
    expect(UserRole.UNIVERSITY_USER).toBe('university_user');
    expect(UserRole.UNIVERSITY_ADMIN).toBe('university_admin');
  });
});

describe('User Type', () => {
  test('should create a valid User object', () => {
    const user: User = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      type: UserRole.SUPER_USER,
      createdOn: new Date(),
      university: 101,
      universityName: 'Test University',
    };

    expect(user.id).toBe(1);
    expect(user.firstName).toBe('John');
    expect(user.lastName).toBe('Doe');
    expect(user.email).toBe('john.doe@example.com');
    expect(user.type).toBe(UserRole.SUPER_USER);
    expect(user.university).toBe(101);
    expect(user.universityName).toBe('Test University');
    expect(user.createdOn).toBeInstanceOf(Date);
  });
});

describe('PatchUserRequestPayload Type', () => {
  test('should allow partial updates', () => {
    const payload: PatchUserRequestPayload = {
      firstName: 'Jane',
    };

    expect(payload.firstName).toBe('Jane');
    expect(payload.lastName).toBeUndefined();
  });
});

describe('CreatePersonForm Interface', () => {
  test('should create a valid CreatePersonForm object', () => {
    const form: CreatePersonForm = {
      firstName: 'Jane',
      lastName: 'Doe',
      email: 'jane.doe@example.com',
      university: { label: 'Test University', id: 101 },
      type: UserRole.UNIVERSITY_ADMIN,
    };

    expect(form.firstName).toBe('Jane');
    expect(form.lastName).toBe('Doe');
    expect(form.email).toBe('jane.doe@example.com');
    expect(form.university).toEqual({ label: 'Test University', id: 101 });
    expect(form.type).toBe(UserRole.UNIVERSITY_ADMIN);
  });

  test('should allow null for university', () => {
    const form: CreatePersonForm = {
      firstName: 'Jane',
      lastName: 'Doe',
      email: 'jane.doe@example.com',
      university: null,
      type: UserRole.UNIVERSITY_ADMIN,
    };

    expect(form.university).toBeNull();
  });
});

describe('EditFavoritesRequestPayload Interface', () => {
  test('should create a valid EditFavoritesRequestPayload object', () => {
    const payload: EditFavoritesRequestPayload = {
      consumerUnitId: 123,
      action: 'add',
      personId: '456',
    };

    expect(payload.consumerUnitId).toBe(123);
    expect(payload.action).toBe('add');
    expect(payload.personId).toBe('456');
  });
});

describe('EditPasswordRequestPayload Interface', () => {
  test('should create a valid EditPasswordRequestPayload object', () => {
    const payload: EditPasswordRequestPayload = {
      currentPassword: 'oldPassword123',
      newPassword: 'newPassword123',
      confirmPassword: 'newPassword123',
    };

    expect(payload.currentPassword).toBe('oldPassword123');
    expect(payload.newPassword).toBe('newPassword123');
    expect(payload.confirmPassword).toBe('newPassword123');
  });

  test('should validate password matching', () => {
    const payload: EditPasswordRequestPayload = {
      currentPassword: 'oldPassword123',
      newPassword: 'newPassword123',
      confirmPassword: 'newPassword123',
    };

    expect(payload.newPassword).toBe(payload.confirmPassword);
  });
});
