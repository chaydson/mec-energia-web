import { getHeadTitle } from "../../src/utils/head";

describe('getHeadTitle', () => {
  it('should return the default title when no title is provided', () => {
    expect(getHeadTitle()).toBe("MEPA - Monitoramento de Energia em Plataforma Aberta");
  });

  it('should handle an empty string as a provided title', () => {
    expect(getHeadTitle("")).toBe("MEPA - Monitoramento de Energia em Plataforma Aberta");
  });

  it('should remove unnecessary spaces before or after the title and concatenate the default title', () => {
    expect(getHeadTitle("  CEB - Distribuidora  ")).toBe("CEB - Distribuidora | MEPA - Monitoramento de Energia em Plataforma Aberta");
  });
});