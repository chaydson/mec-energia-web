import {
  sendFormattedDate,
  monthYearForPlot,
  monthYear,
  getFormattedDate,
  getFormattedDateUTC,
  getFormattedDateAndTime,
  getTimeFromDateUTC,
  getMonthFromNumber,
  getActualYear
} from '../../src/utils/date';
import '@testing-library/jest-dom';

describe('Test Date Functions', () => {
  it('tests sendFormattedDate with date object', () => {
    const testedDate = new Date("2021-05-02T00:00:00");
    const formattedExpected = "2021-05-02";
    expect(sendFormattedDate(testedDate)).toBe(formattedExpected);
  });

  it('tests monthYearForPlot function', () => {
    const result = monthYearForPlot("2021-05-02");
    expect(result).toEqual(["Mai", "2021"]);
  });

  it('tests monthYear function', () => {
    const result = monthYear("2021-05-02");
    expect(result).toBe("Mai 2021");
  });

  it('tests getFormattedDate with a valid string', () => {
    expect(getFormattedDate("2021-05-02")).toBe("02/05/2021");
  });

  it('tests getFormattedDate with null input', () => {
    expect(getFormattedDate(null)).toBeNull();
  });

  it('tests getFormattedDateUTC with a valid string', () => {
    const result = getFormattedDateUTC("2021-05-02");
    expect(result).toEqual(new Date(2021, 4, 2)); // Mês começa em 0
  });

  it('tests getFormattedDateUTC with null input', () => {
    expect(getFormattedDateUTC(null)).toBeNull();
  });

  it('tests getFormattedDateAndTime with default format', () => {
    const result = getFormattedDateAndTime("2021-05-02T14:30:00");
    expect(result).toBe("02/05/2021 02h30");
  });

  it('tests getFormattedDateAndTime with custom format', () => {
    const result = getFormattedDateAndTime("2021-05-02T14:30:00", "yyyy-MM-dd HH:mm");
    expect(result).toBe("2021-05-02 14:30");
  });

  it('should handle invalid date input by returning NaN', () => {
    const stringDate = 'invalid-date-string';

    const result = getTimeFromDateUTC(stringDate);

    expect(result).toBeNaN();
  });

  it('tests getMonthFromNumber without capitalization', () => {
    const result = getMonthFromNumber(4, 2021);
    expect(result).toBe("maio");
  });

  it('tests getMonthFromNumber with capitalization', () => {
    const result = getMonthFromNumber(4, 2021, true);
    expect(result).toBe("Maio");
  });

  it('tests getActualYear', () => {
    const currentYear = new Date().getUTCFullYear();
    expect(getActualYear()).toBe(currentYear);
  });
});




