import { tariffFlags, getTariffFlagLabel, getTariffFlagName, minimumDemand } from "../../src/utils/tariff";

describe('tariffFlags', () => {
  it('should contain only the keys "G" and "B"', () => {
    const keys = Object.keys(tariffFlags);
    expect(keys).toEqual(["G", "B"]);
  });

  it('should map "G" to "Verde" and "B" to "Azul"', () => {
    expect(tariffFlags.G).toBe("Verde");
    expect(tariffFlags.B).toBe("Azul");
  });
});

describe('getTariffFlagLabel', () => {
  it('should return the correct label for "G"', () => {
    expect(getTariffFlagLabel("G")).toBe("Verde");
  });

  it('should return the correct label for "B"', () => {
    expect(getTariffFlagLabel("B")).toBe("Azul");
  });
});

describe('getTariffFlagName', () => {
  it('should return "Azul" for "B"', () => {
    expect(getTariffFlagName("B")).toBe("Azul");
  });

  it('should return "Verde" for "G"', () => {
    expect(getTariffFlagName("G")).toBe("Verde");
  });

  it('should return undefined for invalid tariff flag', () => {
    expect(getTariffFlagName("X")).toBeUndefined();
  });
});

describe('minimumDemand', () => {
  it('should have a minimum value of 30', () => {
    expect(minimumDemand.value).toBe(30);
  });

  it('should have the correct error message', () => {
    expect(minimumDemand.message).toBe("Insira um valor maior ou igual a 30");
  });
});
