import isValidCnpj from "../../../src/utils/validations/isValidCnpj";

describe("isValidCnpj", () => {
  it("deve retornar false para CNPJ com caracteres não numéricos", () => {
    expect(isValidCnpj("12.345.678/0001-AA")).toBe(false);
  });

  it("deve retornar false para CNPJ com menos de 14 dígitos", () => {
    expect(isValidCnpj("12345678")).toBe(false);
  });

  it("deve retornar false para CNPJ com mais de 14 dígitos", () => {
    expect(isValidCnpj("123456789012345")).toBe(false);
  });

  it("deve retornar false para CNPJ com dígitos verificadores incorretos", () => {
    expect(isValidCnpj("12.345.678/0001-91")).toBe(false); // Exemplo de CNPJ inválido
  });

  it("deve retornar true para um CNPJ válido", () => {
    expect(isValidCnpj("12.345.678/0001-95")).toBe(true); // Exemplo de CNPJ válido
  });

  it("deve retornar false para CNPJ que falha no primeiro dígito verificador", () => {
    expect(isValidCnpj("12.345.678/0001-00")).toBe(false);
  });

  it("deve retornar false para CNPJ que falha no segundo dígito verificador", () => {
    expect(isValidCnpj("12.345.678/0001-90")).toBe(false);
  });
});
