import { formatMoney, formatNumber, formatToPtBrCurrency, formatNumberConditional } from '../../src/utils/number';

const moneyFormatter = new Intl.NumberFormat("pt-BR", {
  style: "currency",
  currency: "BRL",
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

describe('Number formatting functions', () => {
  describe('formatMoney', () => {
    it('should format a number as Brazilian currency', () => {
      const money = 1234.56;
      const result = formatMoney(money);
      let expected = moneyFormatter.format(money);
      expect(result).toEqual(expected);
    });

    it('should handle negative numbers', () => {
      const money = -1234.56;
      let expected = moneyFormatter.format(money);
      const result = formatMoney(money);
      expect(result).toEqual(expected);
    });

    it('should handle zero', () => {
      const money = 1;
      let expected = moneyFormatter.format(money);
      const result = formatMoney(money);
      expect(result).toEqual(expected);
    });
  });

  describe('formatNumber', () => {
    it('should format a number with 2 decimal places', () => {
      const number = 1234.56;
      const result = formatNumber(number);
      expect(result).toBe('1.234,56');
    });

    it('should return default output when the input is null', () => {
      const result = formatNumber(null, 'default');
      expect(result).toBe('default');
    });

    it('should handle zero', () => {
      const number = 0;
      const result = formatNumber(number);
      expect(result).toBe('0,00');
    });
  });

  describe('formatToPtBrCurrency', () => {
    it('should use 0 as default decimalPlaces when not provided', () => {
      expect(formatToPtBrCurrency(1234.56)).toBe("1.234,56");
    });

    it('should format a number with the correct decimal places', () => {
      const number = 1234.56;
      const result = formatToPtBrCurrency(number, 2);
      expect(result).toBe('1.234,56');
    });

    it('should return the default output when the number is null', () => {
      const result = formatToPtBrCurrency(null, 2, 'default');
      expect(result).toBe('default');
    });

    it('should handle undefined input', () => {
      const result = formatToPtBrCurrency(undefined, 2, 'default');
      expect(result).toBe('default');
    });

    it('should handle negative numbers', () => {
      const number = -1234.56;
      const result = formatToPtBrCurrency(number, 2);
      expect(result).toBe('-1.234,56');
    });
  });

  describe('formatNumberConditional', () => {
    it('should format a number with 2 decimal places', () => {
      const number = 1234.56;
      const result = formatNumberConditional(number);
      expect(result).toBe('1.234,56');
    });

    it('should return an empty string when the input is null', () => {
      const result = formatNumberConditional(null);
      expect(result).toBe('');
    });

    it('should return an empty string when the input is undefined', () => {
      const result = formatNumberConditional(undefined);
      expect(result).toBe('');
    });

    it('should return an empty string when the input is an empty string', () => {
      const result = formatNumberConditional('');
      expect(result).toBe('');
    });

    it('should handle zero', () => {
      const number = 0;
      const result = formatNumberConditional(number);
      expect(result).toBe('0,00');
    });
  });
});
