import { getVoltageBySubgroup } from "../../src/utils/get-voltage-by-subgroup";

describe('getVoltageBySubgroup', () => {
  it('should return "Inferior a 2,3 kV" for subgroup AS', () => {
    expect(getVoltageBySubgroup("AS")).toBe("Inferior a 2,3 kV");
  });

  it('should return "2,3 kV a 25 kV" for subgroup A4', () => {
    expect(getVoltageBySubgroup("A4")).toBe("2,3 kV a 25 kV");
  });

  it('should return "30 kV a 44 kV" for subgroup A3a', () => {
    expect(getVoltageBySubgroup("A3a")).toBe("30 kV a 44 kV");
  });

  it('should return "69 kV" for subgroup A3', () => {
    expect(getVoltageBySubgroup("A3")).toBe("69 kV");
  });

  it('should return "88 kV a 138 kV" for subgroup A2', () => {
    expect(getVoltageBySubgroup("A2")).toBe("88 kV a 138 kV");
  });

  it('should return "230 kV ou superior" for subgroup A1', () => {
    expect(getVoltageBySubgroup("A1")).toBe("230 kV ou superior");
  });

  it('should return "N/A" for an unknown subgroup', () => {
    expect(getVoltageBySubgroup("UNKNOWN")).toBe("N/A");
  });

  it('should return "N/A" for an empty string', () => {
    expect(getVoltageBySubgroup("")).toBe("N/A");
  });

  it('should return "N/A" for undefined input', () => {
    expect(getVoltageBySubgroup(undefined)).toBe("N/A");
  });

  it('should return "N/A" for null input', () => {
    expect(getVoltageBySubgroup(null)).toBe("N/A");
  });
});
