import { getSubgroup } from "../../src/utils/get-subgroup";

describe('getSubgroup', () => {
  const subgroups = [
    { name: "AS", min: 0, max: 2.3 },
    { name: "A4", min: 2.3, max: 25 },
    { name: "A3a", min: 30, max: 44 },
    { name: "A3", min: 69, max: 69 },
    { name: "A2", min: 88, max: 138 },
    { name: "A1", min: 230, max: Infinity },
  ];

  it('should return the correct subgroup when supplied is within a range', () => {
    expect(getSubgroup(2, subgroups)).toBe("AS");
    expect(getSubgroup(10, subgroups)).toBe("A4");
    expect(getSubgroup(35, subgroups)).toBe("A3a");
    expect(getSubgroup(69, subgroups)).toBe("A3");
    expect(getSubgroup(100, subgroups)).toBe("A2");
  });

  it('should return an empty string if no valid subgroup is found', () => {
    expect(getSubgroup(200, subgroups)).toBe("");
  });
});

describe('getSubgroupWihoutLast', () => {
  const subgroups = [
    { name: "AS", min: 0, max: 2.3 },
    { name: "A4", min: 2.3, max: 25 },
    { name: "A3a", min: 30, max: 44 },
    { name: "A3", min: 69, max: 69 },
    { name: "A2", min: 88, max: 138 },
  ];

  it('should return the last subgroup when supplied is not in the range of one subgroup but it is greater than min (max) of last subgroup', () => {
    // se for >= do que o min, mas < do que o max vai cair no primeiro if e não vai chamar o segundo if
    expect(getSubgroup(88, subgroups)).toBe("A2");
    expect(getSubgroup(89, subgroups)).toBe("A2");

    // testando com um número maior que o max para justamante cobrir o segundo if
    expect(getSubgroup(139, subgroups)).toBe("A2");
  });

});
