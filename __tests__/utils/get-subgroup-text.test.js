import { getSubgroupsText } from "../../src/utils/get-subgroup-text";

describe("getSubgroupsText", () => {
  it("should return a formatted text for valid subgroups", () => {
    const subgroups = [
      { name: "AS", min: 0, max: 2.3 },
      { name: "A4", min: 2.3, max: 25 },
      { name: "A3a", min: 30, max: 44 },
      { name: "A3", min: 69, max: 69 },
      { name: "A2", min: 88, max: 138 },
      { name: "A1", min: 230, max: Infinity },
    ];

    const expectedText = `- 2,3 kV ou inferior
    - De 2,3 kV a 25 kV
    - De 30 kV a 44 kV
    - 69 kV
    - De 88 kV a 138 kV
    - 230 kV ou superior`;

    expect(getSubgroupsText(subgroups)).toBe(expectedText);
  });
})
