import { getSubgroupsText } from "../../src/utils/get-subgroup-text";

describe("getSubgroupsText", () => {
  it("should handle valid subgroups array", () => {
    const invalidSubgroups = [
      { name: "Valid1", min: 0, max: 10 },
      { name: "Valid2", min: 10, max: 20 },
      { name: "Valid3", min: 20, max: 30 },
      { name: "Valid4", min: 30, max: 40 },
      { name: "Valid5", min: 40, max: 50 },
      { name: "Valid6", min: 50, max: Infinity }
    ];

    const expectedText = `- 10 kV ou inferior
    - De 10 kV a 20 kV
    - De 20 kV a 30 kV
    - 30 kV
    - De 40 kV a 50 kV
    - 50 kV ou superior`;

    expect(getSubgroupsText(invalidSubgroups)).toBe(expectedText);
  });
});
